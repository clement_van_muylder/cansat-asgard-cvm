/*
    This sketch just dumps part of a particular EEPROM, for test purposes
*/

#include "ExtEEPROM.h"
#include "SerialStream.h"
#include "Wire.h"

void dumpBuffer(unsigned int startAddress, byte* data, byte size)
{
  char str[10];
  for (int i = 0; i < size; i++) {
    if ((i % 16) == 0) {
      if (i != 0) Serial << ENDL;
      sprintf(str, "  %04x:", startAddress + i);
      Serial << str;
    }
    if ((i % 4) == 0) Serial << "  ";
    sprintf(str, "%02x ", data[i]);
    Serial << str;
  }
  Serial << ENDL;
}

void dumpEEPROM()
{
  const byte bufferSize = 32;
  byte I2C_Address, MSB, LSB;
  unsigned int address;
  unsigned int dataSize;
  byte data[bufferSize];

  Serial.setTimeout(500); // 0.5 sec
  while (Serial.available() > 0) Serial.read();
  Serial << F("I2C Address of EEPROM to dump  (decimal) ? ");
  while (Serial.available() == 0) {
    ;
  }
  I2C_Address = Serial.parseInt();
  Serial << (int) I2C_Address << ENDL;
  while (Serial.available() > 0) Serial.read();
  Serial << F("Start address in EEPROM at I2C address ") << I2C_Address << F(" (hex format, no 0x) ?  ");
  while (Serial.available() == 0) {
    ;
  }
  String str = Serial.readString();
  address = strtol(str.c_str(), 0, 16);
  Serial << F("Ox");
  Serial.println(address, HEX);
  while (Serial.available() > 0) Serial.read();
  Serial << F("How many bytes should I dump ? : ");
  while (Serial.available() == 0) {
    ;
  }
  dataSize = Serial.parseInt();
  Serial << dataSize << ENDL;
  Serial << F("Dumping ") << dataSize << F(" bytes (in hex format) from EEPROM at I2C address ") << I2C_Address
         << F(" starting from address 0x");
  Serial.println(address, HEX);

  // do the job
  unsigned int lastAddress = address + dataSize;
  while (address < lastAddress) {
    byte shunkSize = (lastAddress - address) > bufferSize ? bufferSize : lastAddress - address;
    byte read = ExtEEPROM::readData(I2C_Address, address, data, shunkSize);
    if (read != shunkSize) {
      Serial << F("*** Error: read ") << read << F(" bytes instead of ") << shunkSize << ENDL;
      break;
    }
    dumpBuffer(address, data, read);
    address += read;
  } // while
  Serial << F("End of job.") << ENDL << ENDL;
}

void setup() {
  Serial.begin(19200);
  while (!Serial) ;
  Wire.begin();
  Wire.setClock(400000L);
}

void loop() {
  dumpEEPROM();
}
