/*
    Utility program to toggle the regulator command line
    Only supports Feather board.
*/
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "IsaTwoConfig.h"
#include "elapsedMillis.h"

constexpr bool toggleManually = false;  // if true, toggling when instructed on the serial line, otherwise toggle automatically.
constexpr int  autoTogglePeriod_On = 15; // Toggle period when On in seconds, when toggleManually=false.
constexpr int  autoTogglePeriod_Off = 2; // Toggle period when Off in seconds, when toggleManually=false.

bool imagerOn = false;
elapsedMillis ts;
unsigned long autoTogglePeriod; // in msec.

void toggle() {
  imagerOn = (!imagerOn);
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr, (imagerOn ? LOW : HIGH));
  Serial << "Imager is now " << (imagerOn ? "ON" : "OFF") << ENDL;
  autoTogglePeriod = (imagerOn ? autoTogglePeriod_On : autoTogglePeriod_Off);
  autoTogglePeriod*=1000;
}

void setup() {
  DINIT(115200);
  pinMode(ImagerCtrl_MasterDigitalPinNbr, OUTPUT);
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH);
  Serial << "Imager Ctrl line: " << ImagerCtrl_MasterDigitalPinNbr << ENDL;
  if (toggleManually) {
    Serial << "Enter any character followed by 'Enter' to toggle the imager ctrl line" << ENDL;
    Serial << " Enable is currently LOW" << ENDL;
    while (Serial.available()) {
      Serial.read();
    }
  } else {
    Serial << "Toggling imager ctrl line automatically:" << ENDL;
    Serial << "   On during  " << autoTogglePeriod_On << " sec" << ENDL;
    Serial << "   Off during " << autoTogglePeriod_Off << " sec" << ENDL;
    autoTogglePeriod=autoTogglePeriod_Off;
    ts=0;
  }
}

void loop() {
  if (toggleManually) {
    if (Serial.available()) {
      toggle();
      while (Serial.available()) {
        Serial.read();
      }
    }
  } else {
    if (ts >= autoTogglePeriod) {
      toggle();
      ts=0;
    }
  }
}
