/*
   RF_RealisticTest_Station.ino

   A sketch which simulates the ground station side of the RF transmission:
     - By default listen and check the received records are correct (i.e. same as EXPECTED_RECORD, except
       for timestamp, and timestamps increasing by 1.
     - Every 20 seconds, send a CMD_STRING to interrupt the emission by the can (entering command mode) until
       the acknowledge RSP_STRING is received. When received send a couple of strings and check they
       are properly echoed by the can.

     - The board waits for an actual USB Serial link, unless the SerialActivationPin is pulled down.

*/

#include "DebugCSPU.h"
#include "IsaTwoRecord.h"
#include "elapsedMillis.h"

// Configuration constants
#define EXPECTED_RECORD  "0,0,12345,23456,-30969,-19858,-8747,2355,456.78909,567.89117,678.91229,1,234.56779,345.67889,123.45670,456.78909,345.67889,123.45670,456.78909,234.56779"
#define CMD_STRING "5,12345,67890"
#define RSP_STRING "**Command received**"
#define STRING_TO_BE_ECHOED "--A string to be echoed---"
constexpr byte RF_RxPinOnUno = 9;
constexpr byte RF_TxPinOnUno = 11;
constexpr byte SerialActivationPin=6;  // If this pin is pulled-down the board will wait for Serial port initialisation,
                                      // If it is open or pulled-up, it will not 
//#define SIMULATE_ON_USB_SERIAL // Define to have RF reception and transmission performed on Serial (for debugging)
constexpr byte BufferSize = 200;
constexpr unsigned long cmdPeriod = 15000; // msec
constexpr unsigned long responseDelay = 2000; // msec
constexpr unsigned long RF_BaudRate = 115200; // must be consistent with the RF module config. 9600 is not enough!
constexpr bool showBufferOverlow = true; // Set to true to get messages on Serial when the reception buffer overflows.
constexpr bool showBufferWheneverProcessed = false ; // Set to true to display the buffer content each time it is processed.
constexpr bool showIgnoredMsg = true; // Set to true to display ignored (valid) incoming messages.
constexpr bool showStateChanges = true; // Set to true to display every state change.
constexpr bool printAllCharactersReceived = false; // Set to true to have every single char received from the RF immediately printed on Serial
constexpr bool showInvalidMsg = true; // Set to true to display all invalid messages received.
//------------------------------------------------------------------------------------------

//Define a RF Serial port.
#ifdef SIMULATE_ON_USB_SERIAL
HardwareSerial &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif

// -------------------------- Types -------------------------------
// The messages we recognize
typedef enum MsgType {
  None,
  Record,
  CmdAck,
  EchoedString
} MsgType_t;

// The states
typedef enum State {
  ReceivingRecords,
  WaitingForResponse,
  WaitingForEcho
} State_t;

// -------------------------- Globals -------------------------------
State_t currentState;
elapsedMillis elapsed, heartbeatElapsed;
char buffer[BufferSize]; // The reception buffer.
byte bufferIndex; // index of first free position in buffer.
bool lineReceived; // true if end-of-line was detected.

// -------------------------- End Globals ---------------------------
/* Compare all fields except timestamp are identical
   return timestamp */
bool checkRecord(const char* csvString, bool silent = false) {
  //Serial << "Checking '" << csvString << "'" << ENDL;
  static unsigned long currentTS = 0; // the timestamp expected in next record.

  // extract timestamp, after first comma.
  const char* comma = strchr(csvString, ',');
  if (!comma) return false;
  comma++;
  unsigned long ts = atol(comma);
  comma = strchr(comma, ',');
  if (!comma) return false;
  comma++;
  // now pointer right after the second comma.
  const char* refComma = strchr(EXPECTED_RECORD, ',');
  if ((!refComma) && (!silent)) {
    Serial << F("Error: no comma in EXPECTED_RECORD ?? ") << ENDL;
    return false;
  }
  refComma++;
  refComma = strchr(refComma, ',');

  if ((!refComma) && (!silent)) {
    Serial << F("Error: no 2nd comma in EXPECTED_RECORD ?? ") << ENDL;
    return false;
  }
  refComma++;

  bool equal = (strcmp(comma, refComma) == 0);
  if ((!equal) /* && (!silent) */) {
    Serial << F("Error:") << ENDL;
    Serial << F("  Got     : ") << csvString << ENDL;
    Serial << F("  Expected: ") << EXPECTED_RECORD << ENDL;
    for (int i = 0; i < strlen(comma); i++)
    {
      if (comma[i] != refComma[i]) {
        Serial << "char " << i << " differ: " << comma[i] <<" vs " << refComma[i] << ENDL;
      }
    }
    return false;
  }

  bool result = false;
  if (currentTS == 0) {
    currentTS = ts + 1;
    result = true;
  } else {
    if (ts == currentTS) {
      result = true;
    } else {
      Serial << "Error in timestamp: expected " << currentTS << ", got " << ts << ENDL;
    }
    currentTS = ts + 1;
  }
  return result;
}

/*
   Process buffer content and empty it.
   Return the message received.
   Checks that records have successive timestamps.
*/
MsgType_t processBuffer(bool recordExpected) {
  MsgType_t result = None;

  if ((bufferIndex == 0) || (!lineReceived)) {
    return None;
  }

  if (showBufferWheneverProcessed) {
    Serial << ENDL << F("Processing '") << buffer << "'" << ENDL;
  }

  // First check for record: if received, we're out of cmd mode.
  static int recordCounter = 0;
  if (checkRecord(buffer, !recordExpected)) {
    result = Record;
    recordCounter++;
    Serial << ".";
    if (recordCounter >= 50) {
      recordCounter = 0;
      Serial << ENDL;
    }
  }
  else if (strncmp(buffer, STRING_TO_BE_ECHOED, strlen(STRING_TO_BE_ECHOED)) == 0) {
    recordCounter = 0;
    result = EchoedString;
  }
  else if (strncmp(buffer, RSP_STRING, strlen(RSP_STRING)) == 0) {
    result = CmdAck;
  }
  else {
    // Unrecognized msg
    Serial << 'I';
    if (showInvalidMsg) {
      Serial << ENDL << F("  Invalid msg: '") << buffer << "'" << ENDL;
    }
  }
  bufferIndex = 0; // Reset buffer pointer: content is processed.
  lineReceived = false;
  return result;
}

void documentOnSerial() {
  // Document on Serial
  Serial << F("=== Simulating the RECEIVING STATION ===") << ENDL;
#ifdef SIMULATE_ON_USB_SERIAL
  Serial << F(" === Using USB Serial instead of RF Serial for test! ===") << ENDL;
#else
#endif
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Feather board detected: using RX-TX pins for RF communication" << ENDL;
#else
  Serial << "Assuming AVR board: using softwareSerial (rx=" << RF_RxPinOnUno
         << ", tx=" << RF_TxPinOnUno << ") for RF communication" << ENDL;
#endif
  Serial << ENDL;
  Serial << F("RF baud rate   : ") << RF_BaudRate << F(" baud") << ENDL;
  Serial << F("Expected record: '") << EXPECTED_RECORD << "'" << ENDL;
  int expectedStringLength = strlen(EXPECTED_RECORD);
  Serial << F("Expecting records using ") << expectedStringLength
         << F(" characters") << ENDL;
  Serial << F("Reception buffer size: ") << BufferSize;
  if (expectedStringLength > (BufferSize - 20)) {
    Serial << F(": *** add some buffer margin! ***") << ENDL;
  } else {
    Serial << F(": should be OK.") << ENDL;
  }
  Serial << F("Sending command every ") << cmdPeriod / 1000.0 << F(" sec") << ENDL;
  Serial << F("A '.' denotes are valid record received, an 'I' denotes an invalid message") << ENDL;
}

/*
    Listen to RF port. This function expects the message buffer to be empty
*/
void listenToRF() {
  //assert(bufferIndex == 0);
  // Always receive until queue is empty or end-of-message.
  while (RF.available()) {
    char c = RF.read();
    if (printAllCharactersReceived) {
      Serial << '/' << c << '/';
    }
    if (bufferIndex < (BufferSize - 1)) {
      if ((c == 10) || (c == 13) || (c == '\0')) {
        // This is and end-of-message token.
        // '\n' is usually both CR  (10) and LF (13).
        if (bufferIndex != 0) {
          buffer[bufferIndex++] = '\0'; // end of string marker.
          lineReceived = true;
          return; // stop reading as soon as a message is received.
        }
        else
        {
          // just ignore end-of-string if empty buffer.
        }
      } else {
        buffer[bufferIndex++] = c;
      }
    } else {
      // Buffer overflow
      if (showBufferOverlow) {
        Serial << "Error: buffer overflow" << ENDL;
      }
      bufferIndex = 0;
    }
  } // While RF.available();
} // listenToRF()


void switchTo(State_t newState) {
  currentState = newState;
  if (showStateChanges) {
    Serial << F("Changing state to ");
    switch (newState) {
      case ReceivingRecords:
        Serial << F("ReceivingRecords");
        break;
      case WaitingForResponse:
        Serial << F("WaitingForResponse");
        break;
      case WaitingForEcho:
        Serial << F("WaitingForEcho");
        break;
      default:
        Serial << "** Unknown ** (" << newState << F(")");
    } // switch
    Serial << ENDL;
  } // if show
  elapsed = 0;
}


void ignoreMessage(MsgType_t msgType)
{
  if (showIgnoredMsg) {
    switch (msgType) {
      case None:
        break;
      case Record:
        Serial << F("Ignored incoming record") << ENDL;
        break;
      case CmdAck:
        Serial << F("Ignored cmd response") << ENDL;
        break;
      case EchoedString:
        Serial << F("Ignored echoed string") << ENDL;
        break;
      default:
        Serial << F("*** Error: ignored unexpected msgType: ") << msgType << ENDL;
    } // switch
  } // if show
} // ignoreMsg

void sendStringToEcho() {
	RF << STRING_TO_BE_ECHOED << ENDL;
	Serial << "String sent for echo" << ENDL;
}

void sendCommand() {
	RF << CMD_STRING << ENDL;
	Serial << "Command sent" << ENDL;
}

void doProcess()
{
  static int counter = 0;
  MsgType_t msgReceived = processBuffer(currentState == ReceivingRecords);
  switch (currentState) {
    case ReceivingRecords:
      if (msgReceived == Record) {
        // Do not react. 
      } else {
        ignoreMessage(msgReceived);
      }
      if (elapsed >= cmdPeriod) {
		sendCommand();
        counter = 0;
        switchTo(WaitingForResponse);
      }
      break;
    case WaitingForResponse:
      switch (msgReceived) {
        case Record:
          // Do not react.
          break;
        case CmdAck:
          Serial << F("Response received.") << ENDL;
          sendStringToEcho();
          counter = 0;
          switchTo(WaitingForEcho);
          break;
        default:
          ignoreMessage(msgReceived);
      }
      // We could have changed state already
      if ((currentState == WaitingForResponse) && (elapsed >= responseDelay)) {
        if (counter < 3) {
          sendCommand();
          counter++;
          elapsed = 0;
        } else {
          // retry exhausted.
          switchTo(ReceivingRecords);
        }
      }
      break;
    case WaitingForEcho:
      switch (msgReceived) {
        case Record:
          // Do not react
          break;
        case EchoedString:
          Serial << F("Echo received") << ENDL;
          if (counter < 3) {
			sendStringToEcho();
            counter++;
            elapsed = 0;
          } else {
            // Done Echoing
            switchTo(ReceivingRecords);
          }
          break;
        default:
          ignoreMessage(msgReceived);
      }
      // We could have changed state already
      if ((currentState == WaitingForEcho) && (elapsed >= responseDelay)) {
        if (counter < 3) {
          sendStringToEcho();
          counter++;
          elapsed = 0;
        } else {
          // retry exhausted.
          switchTo(ReceivingRecords);
        }
      }
      break;
    default:
      Serial << F("*** Error: unexpected state: ") << currentState << ENDL;
  }  // switch(state)
} // doProcess()

void setup() {
  DINIT_IF_PIN(115200,SerialActivationPin);
  documentOnSerial();

  RF.begin(RF_BaudRate);
  pinMode(LED_BUILTIN, OUTPUT);
  currentState = ReceivingRecords;
  elapsed = 0;
  heartbeatElapsed = 0;
  lineReceived = false;

  Serial << F("Setup complete") << ENDL;
}

void loop() {
  listenToRF();
  doProcess();
  if (heartbeatElapsed > 500) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    heartbeatElapsed = 0;
  }
}
