
/*
 * This utility program writes NUM_RECORDS records in the EEPROMs and times the operation.
 *
 */

#include "HardwareScanner.h"
#include "EEPROM_BankWriter.h"
#include "SerialStream.h"

const byte MinRecordSize=100;
const byte MaxRecordSize=255;
const byte RecordSizeIncrement=10; 
const unsigned long NUM_RECORDS = 100; 

EEPROM_BankWriter::EEPROM_Key testKey=0x6678;
int errorCount=0;

byte  buffer[MaxRecordSize];
byte referenceRecord[MaxRecordSize];
HardwareScanner hw;
EEPROM_BankWriter::EEPROM_Header previousHeader;

void printHeader(EEPROM_BankWriter::EEPROM_Header h) {
	Serial << "firstFreeChip=" << h.firstFreeChip << ", firstFreeByte=" <<h.firstFreeByte << ENDL;
}

void initReferenceRecord() {
	for (int i = 0; i< MaxRecordSize;i++)
		referenceRecord[i]=i;
}

void testOneRecordSize(const byte size)
{
  EEPROM_BankWriter eb(5);
  Serial<< F("------------------------------------") << ENDL;
  Serial << F("Testing with record of size ") << size << ENDL;
  bool result = eb.init(testKey+size, hw, size);
  if (!result) {
    Serial << F("test_EEPROM_Bank_Perfo: ERROR INITIALIZING EEPROM") << ENDL;
    exit(-1);
  }
  unsigned long numFree = eb.getNumFreeRecords();
  if (numFree == 0) {
  	  Serial << F("Error: numFree = 0. EEPROM bank should be empty...") << ENDL;
  	  errorCount++;
  }

 unsigned long count=0;
 unsigned long duration=0;
  while ((count < NUM_RECORDS) )
  {
    unsigned long ts=millis();
    eb.storeOneRecord(referenceRecord, size);
    duration+=millis()- ts;
    if ((count % 100) == 0) {
      Serial << ".";
      Serial.flush();
      eb.doIdle(true);
    }
    count++;
  }
  eb.doIdle(true);
  Serial << ENDL << "Wrote " << count << " records in " << duration << " msec" << ENDL;
  Serial << "i.e. " << ((float) duration)/count  << " msec/record" << ENDL;
}

void setup() {
  Serial.begin (19200);
  while (!Serial) ;
  delay(1000); 
  Serial << "Starting test..." << ENDL;
  Serial.flush();
  hw.init(0x50, 0x57);
  hw.printFullDiagnostic(Serial);

  initReferenceRecord();
  previousHeader.firstFreeChip=99;

  // NB: do not use a byte as loop index: it would overflow if MaxRecordSize == 255.
  for (int size=MinRecordSize; size<=MaxRecordSize; size+=RecordSizeIncrement) {
	  	testOneRecordSize((byte) size);
  }

  if (errorCount) {
	  Serial  << F("**** ") << errorCount << F(" errors detected") << ENDL;
  }
  else {
	  Serial << F("No error detected")<< ENDL;
  }
  Serial << F("End of job") << ENDL;

}

void loop() {
  // put your main code here, to run repeatedly:

}
