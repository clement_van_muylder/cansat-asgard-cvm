/*
 * GMiniConfig.h
 *
 * Specific configuration for the G-Mini project
 */

#pragma once
#include "CansatConfig.h"
#include "CansatInterface.h"
#include <type_traits>

//------------------------ RF links Configuration --------------------------

// Pair used for MainCan and Ground is defined in CansatConfig
// Warning: the Ground Xbee should be consistent with the one defined in
//          CansatConfig.h, since the RF-Transceiver takes its configuration
//			from there.
#if (RF_XBEE_MODULES_SET=='A') // We use set the A pair for can & ground.
constexpr uint32_t GM_MainCan_XBeeAddressSH=XBeeAddressSH_01;
constexpr uint32_t GM_MainCan_XBeeAddressSL=XBeeAddressSL_01;
constexpr uint32_t GM_SubCan1_XBeeAddressSH=XBeeAddressSH_03;
constexpr uint32_t GM_SubCan1_XBeeAddressSL=XBeeAddressSL_03;
constexpr uint32_t GM_SubCan2_XBeeAddressSH=XBeeAddressSH_05;
constexpr uint32_t GM_SubCan2_XBeeAddressSL=XBeeAddressSL_05;
constexpr uint32_t GM_Ground_XBeeAddressSH=XBeeAddressSH_02;
constexpr uint32_t GM_Ground_XBeeAddressSL=XBeeAddressSL_02;
constexpr uint32_t GM_Ground2_XBeeAddressSH=XBeeAddressSH_08;
constexpr uint32_t GM_Ground2_XBeeAddressSL=XBeeAddressSL_08;
constexpr uint32_t GM_SubCan3_XBeeAddressSH=XBeeAddressSH_06; // module 6 defective
constexpr uint32_t GM_SubCan3_XBeeAddressSL=XBeeAddressSL_06;
constexpr uint32_t GM_SubCan4_XBeeAddressSH=XBeeAddressSH_04; // module 4 defective
constexpr uint32_t GM_SubCan4_XBeeAddressSL=XBeeAddressSL_04;
#elif (RF_XBEE_MODULES_SET=='B')  // We use pair B for Can & Ground.
constexpr uint32_t GM_MainCan_XBeeAddressSH=XBeeAddressSH_03;
constexpr uint32_t GM_MainCan_XBeeAddressSL=XBeeAddressSL_03;
constexpr uint32_t GM_SubCan1_XBeeAddressSH=XBeeAddressSH_01;
constexpr uint32_t GM_SubCan1_XBeeAddressSL=XBeeAddressSL_01;
constexpr uint32_t GM_SubCan2_XBeeAddressSH=XBeeAddressSH_08;
constexpr uint32_t GM_SubCan2_XBeeAddressSL=XBeeAddressSL_08;
constexpr uint32_t GM_Ground_XBeeAddressSH=XBeeAddressSH_04;
constexpr uint32_t GM_Ground_XBeeAddressSL=XBeeAddressSH_04;
constexpr uint32_t GM_Ground2_XBeeAddressSH=XBeeAddressSH_02;
constexpr uint32_t GM_Ground2_XBeeAddressSL=XBeeAddressSL_02;
constexpr uint32_t GM_SubCan3_XBeeAddressSH=XBeeAddressSH_06;
constexpr uint32_t GM_SubCan3_XBeeAddressSL=XBeeAddressSL_06;
constexpr uint32_t GM_SubCan4_XBeeAddressSH=XBeeAddressSH_05;
constexpr uint32_t GM_SubCan4_XBeeAddressSL=XBeeAddressSL_05;
#elif (RF_XBEE_MODULES_SET=='C') // We use par C for Can & Ground
constexpr uint32_t GM_MainCan_XBeeAddressSH=XBeeAddressSH_05;
constexpr uint32_t GM_MainCan_XBeeAddressSL=XBeeAddressSL_05;
constexpr uint32_t GM_SubCan1_XBeeAddressSH=XBeeAddressSH_03;
constexpr uint32_t GM_SubCan1_XBeeAddressSL=XBeeAddressSL_03;
constexpr uint32_t GM_SubCan2_XBeeAddressSH=XBeeAddressSH_04;
constexpr uint32_t GM_SubCan2_XBeeAddressSL=XBeeAddressSL_04;
constexpr uint32_t GM_Ground_XBeeAddressSH=XBeeAddressSH_06;
constexpr uint32_t GM_Ground_XBeeAddressSL=XBeeAddressSH_06;
constexpr uint32_t GM_Ground2_XBeeAddressSH=XBeeAddressSH_02;
constexpr uint32_t GM_Ground2_XBeeAddressSL=XBeeAddressSL_02;
constexpr uint32_t GM_SubCan3_XBeeAddressSH=XBeeAddressSH_08;
constexpr uint32_t GM_SubCan3_XBeeAddressSL=XBeeAddressSL_08;
constexpr uint32_t GM_SubCan4_XBeeAddressSH=XBeeAddressSH_01;
constexpr uint32_t GM_SubCan4_XBeeAddressSL=XBeeAddressSL_01;
#else
#error "Unexpected RF_XBEE_MODULE_SET in GminiConfig.h"
#endif

/** @ingroup GMiniCSPU
 *  An enum identifying the various RF communication strategies
 *  considered for the GMini project.
 */
enum class GMiniRF_Strategy {
	MainCanAsRepeater = 1,		/**< The main can receives transmissions
									 from the subcans and sends them
									 further to the ground */
	BroadcastFromSubcans = 2,   /**< The subcans use broadcast transmissions
									 to reach both the main can and the ground.*/
	MulticastFromSubcans = 3,	/**< The subcans use multicast transmissions to
									 a group including the main can and the ground */
	MultipleBindingsFromSubcans = 4,
								/**< The subcans use multiple bindings to reach
									 both the main can and the ground. */
	DoubleTransmissionFromSubcans=5
								/**< The subcans send 2 unicast transmissions
								     for each record (one to main can, one to ground */
};

/** @ingroup GMiniCSPU
 * The RF Strategy currently in use */
constexpr GMiniRF_Strategy GMiniSelectedRF_Strategy=
			GMiniRF_Strategy::DoubleTransmissionFromSubcans;

/** @ingroup GMiniCSPU
 *  This pin defines whether the subcan is SubCan1 (if the pin is open) or
 *  SubCan2 (if the pin is wired to the ground).
 *  NB: Use any otherwise unused digital pin (except pin 5 which is output only */
constexpr byte SubCanID_Pin = 7;


// GMiniSecondaryMissionController
constexpr unsigned int MaxDelayFromTakeoffToEjection = 15000; /**< over this limit in msec, the sub can will be ejected*/
constexpr uint8_t VelocityAveragingPeriod = 3; /**< sec*/ 
constexpr float VelocityThresholdForTakeoff = -7.00;/**< m/s positive when falling but during the takeoff we are going up so it is negative*/

// ServoLatch configuration
constexpr uint8_t ServoLatchPWM_Pin = 5; /**< number of the PWM pin for the servo */
constexpr uint8_t ServoLatchActivationPin = 6; /**< Number of the pin used to enable the H-bridge to work with the ServoLatch.
 	 	 	 	 	 	 	 	 	 	 	 	 	Pin Enable2 of H-bridge */
constexpr int ServoLatchNeutralPosition = 79; /**< number in degree of the neutral position of the servo*/
constexpr int ServoLatchAmplitude = 30; /**< The amplitude in degrees for the move of the ServoLatch from unlock to lock */
constexpr int ServoLatchMinPulseWidth = 544;   /**< Minimum pulse width for servo latch */
constexpr int ServoLatchMaxPulseWidth = 2400; /**< Maximum pulse width for servo latch */

// ScrewLatch configuration
constexpr uint8_t ScrewLatchPWM_Pin = 5; /**< number of the PWM pin for the ScrewLatch (Enable1 pin of H-Bridge) */
constexpr uint8_t ScrewLatchForward_Pin = 9; /**< Number of the pin connected to pin #2 (input1) of the H-bridge to work with the ServoLatch */
constexpr uint8_t ScrewLatchReverse_Pin = 12; /**< Number of the pin connected to pin #7 (input2) of the H-bridge to work with the ServoLatch */
