/*
   GMiniSecondaryMissionController.h
*/

// This class is not supported on SAMD51 (e.g. ItsyBisty M4) because servos are not
// supported (as of 202203) on SAMD51 boards.
#ifndef __SAMD51__

#pragma once
#include "float.h"
#include "SecondaryMissionController.h"
#include "GMiniRecord.h"
#include "ServoLatch.h"
#include "GMiniConfig.h"
#include "BMP_Client.h"

/** @brief our secondary mission manager for the Gmini Project
This class can : 
- use the velocity in the record to make a moving average
-check if the velocity in the record are valid
- check if the average is valid 
- put the velocity values in a table and clear it if he receives an invalid velocity
- detect the takeoff
- trigger the ejection
By doing this last action, this class can actually modify one of the data contained in the records.
*/
class GMiniSecondaryMissionController: public SecondaryMissionController {

  public:
    GMiniSecondaryMissionController() : latch(nullptr) {
    
   };
    /** Initialize the controller before use
     *  @return true if initialization is successful, false otherwise
     */
    bool begin() override;
   
    void manageSecondaryMission(CansatRecord & record) override;
    Latch * getLatch() {
      return latch;
    };


    float getAverageVelocity(); /**< return the velocity average if it is valid **/
    static constexpr uint8_t NumberOfVelocitySamples = (VelocityAveragingPeriod * 1000 / CansatAcquisitionPeriod) + 1; /**< this is the number of the samples in order to fill entirely a table and get a valid velocity average */
    
    static constexpr float InvalidVelocity = FLT_MAX;
    bool takeoffDetected(); /**< true if the takeoff has been detected */
  protected:

  private:
/** this method stores the velocity within the record in a table and averages it. Then, it checks if velocities got are valid and then allows us to know if the takeoff has already happened. 
@param record containing the descent velocity
*/
    void updateStatus(GMiniRecord & record);
    void clearAverageVelocity(); /**< clear up all data of the table */
   /** if true one of the conditions for ejecting is fulfilled
   @param  record The record containing the current timestamp
 */
    bool mustEject(GMiniRecord & record); 
    void printVelocityTable(); /**< it prints the table with the descent velocity variable within the record */


    bool subCanEjected; /**< if true, the can is ejected */
    Latch* latch;
    unsigned long takeoffTimestamp; /**< timestamp of the takeoff */
    float velocityTable[NumberOfVelocitySamples] = {}; /**< table used to store the velocity values of the record */
    float averageVelocity = 0; /**< current average of the velocity in the table*/
    uint8_t currentVelocityIndex = 0; /**< indicates which index or row of the table we are in */
    bool velocityAverageValid; /**< if true when the table is fully filled with  valid velocity values */
    unsigned long lastRecordTimestamp = 0; /**< timestamp of the current last record */
    bool takeoffHappened; /**< if true the takeoff of the rocket is detected */
    
};

#endif
