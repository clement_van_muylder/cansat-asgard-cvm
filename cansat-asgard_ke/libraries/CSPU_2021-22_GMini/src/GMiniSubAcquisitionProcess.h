/*
   GMiniSubAcquisitionProcess.h
*/

#pragma once
#include "GMiniAcquisitionProcess.h"
#include "GMiniRecord.h"
#include "GMiniConfig.h"

/** @brief Acquisition process of the subcan of GMini project
*/
class GMiniSubAcquisitionProcess: public GMiniAcquisitionProcess {
  public:
    virtual void init() override {
    	GMiniAcquisitionProcess::init();
    	pinMode(SubCanID_Pin, INPUT_PULLUP);
    }
    virtual ~GMiniSubAcquisitionProcess() {};
    virtual CansatCanID getCanID() {
    	CansatCanID id = (digitalRead(SubCanID_Pin)==HIGH ?
			CansatCanID::SubCan1 :
			CansatCanID::SubCan2);
    	return id;};

  protected:

    virtual void transmitRecordToXBeeModule(uint32_t destSH=0, uint32_t destSL=0) override {
    	auto xb=getHardwareScanner()->getRF_XBee();
    	if (!xb) return;
    	bool wasAsleep;
    	switch(GMiniSelectedRF_Strategy) {
    	case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
    		wasAsleep=xb->wakeUpXBeeModule();
    		GMiniAcquisitionProcess::transmitRecordToXBeeModule(destSH,destSL);
    		GMiniAcquisitionProcess::transmitRecordToXBeeModule(GM_MainCan_XBeeAddressSH,GM_MainCan_XBeeAddressSL);
    		if (wasAsleep) xb->putXBeeModuleToSleep(); // Garanties that the module will not go to sleep
    										// between transmissions, and is only put to sleep
    										// if we actually woke it up.
    		break;
    	default:
    		GMiniAcquisitionProcess::transmitRecordToXBeeModule(destSL,destSH);
    		Serial << "*** Error: unsupported RF Strategy" << ENDL;
    	};
    }
  private:
    virtual void initSpecificProject () override {
      // Perform specific project initialisation here
    };

    /** Since there is no on-board storage in sub cans, all records are
     *  considered non relevant for storage. This avoids trying to look
     *  for a non-existant SD card.
     */
    virtual bool isRecordRelevantForStorage() const  override { return false;}  ;

    virtual void acquireSecondaryMissionData (CansatRecord & record) override {
      GMiniRecord& recordGM = ((GMiniRecord&)record);
      recordGM.sourceID = getCanID();
      recordGM.subCanEjected = false;
      } ;
};
