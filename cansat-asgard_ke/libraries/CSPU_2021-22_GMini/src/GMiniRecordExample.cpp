/*
 * GMiniRecordExample.cpp
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GMiniRecordExample.h"

void GMiniRecordExample::initValues() {
		timestamp=654321;
		sourceID=(CansatCanID) 23;
		subCanEjected=true;
		newGPS_Measures=true;
		GPS_LatitudeDegrees=-76.543211;
		GPS_LongitudeDegrees=167.897654;
		GPS_Altitude=789.2;
#ifdef INCLUDE_GPS_VELOCITY
     GPS_VelocityKnots=123.45;      	/**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
     GPS_VelocityAngleDegrees=-45.67; /**< Direction of velocity in decimal degrees, 0 = North */
#endif
		temperatureBMP=-1.2;
		pressure=1011.4;
		altitude=1234.6;
#ifdef INCLUDE_REFERENCE_ALTITUDE
		refAltitude=345.6;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
		descentVelocity=243.2341;
#endif
		temperatureThermistor1=4.4;
#ifdef INCLUDE_THERMISTOR2
		temperatureThermistor2=25.5;
#endif
#ifdef INCLUDE_THERMISTOR3
		temperatureThermistor3=12.3;
#endif
	}

template<class T>
static bool checkFloat(T actual, T expected, T accuracy=0.00001) {
	if (fabs(actual - expected) < accuracy){
		return true;
	}
	else {
		Serial << " *** ERROR: float does not match (accuracy=";
		Serial.print(accuracy, 8);
		Serial << "). Got ";
		Serial.print(actual,8);
		Serial << " instead of ";
		Serial.print(expected, 8);
		Serial << ENDL;
		return false;
	}
}

bool GMiniRecordExample::checkValues(bool ignoreTimestamp) {
	uint8_t errors = 0;
	if ((!ignoreTimestamp) && (timestamp != 654321)) {
		errors++;
		Serial << " *** Error in timestamp" << ENDL;
	}
	if (subCanEjected != true) {
			errors++;
			Serial << " *** Error in subCanEjected" << ENDL;
		}
	if (newGPS_Measures != true) {
		errors++;
		Serial << " *** Error in newGPS_Measures" << ENDL;
	}
	if (!checkFloat(GPS_Altitude,789.2f, 0.01f)) {
		errors++;
		Serial << " *** Error in GPS_Altitude" << ENDL;
	}
	if (!checkFloat(GPS_LatitudeDegrees , -76.543211, 1.0E-5)) {
		errors++;
		Serial << " *** Error in GPS_LatitudeDegrees" << ENDL;
	}
	if (!checkFloat(GPS_LongitudeDegrees, 167.897654, 1E-5)) {
		errors++;
		Serial << " *** Error in GPS_LongitudeDegrees" << ENDL;
	}
#ifdef INCLUDE_GPS_VELOCITY
	if (!checkFloat(GPS_VelocityKnots , 123.45f, 0.01f)) {
		errors++;
		Serial << " *** Error in GPS_VelocityKnots" << ENDL;
	}
	if (!checkFloat(GPS_VelocityAngleDegrees , -45.67f, 0.01f)) {
		errors++;
		Serial << " *** Error in GPS_VelocityAngleDegrees" << ENDL;
	}
#endif

	if (!checkFloat(altitude , 1234.6f, 0.1f)) {
		errors++;
		Serial << " *** Error in altitude" << ENDL;
	}
	if (!checkFloat(pressure , 1011.4f, 0.1f)) {
		errors++;
		Serial << " *** Error in pressure" << ENDL;
	}
#ifdef INCLUDE_REFERENCE_ALTITUDE
	if (!checkFloat(refAltitude , 345.6f, 0.1f)) {
		errors++;
		Serial << " *** Error in reference altitude" << ENDL;
	}
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	if (!checkFloat(descentVelocity , 243.23f, 0.01f)) {
		errors++;
		Serial << " *** Error in descentVelocity" << ENDL;
	}
#endif

	if (!checkFloat(temperatureBMP , -1.2f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureBMP" << ENDL;
	}
	if (!checkFloat(temperatureThermistor1 , 4.4f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureThermistor1" << ENDL;
	}
#ifdef INCLUDE_THERMISTOR2
	if (!checkFloat(temperatureThermistor2 , 25.5f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureThermistor2" << ENDL;
	}
#endif
#ifdef INCLUDE_THERMISTOR3
	if (!checkFloat(temperatureThermistor3 , 12.3f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureThermistor3" << ENDL;
	}
#endif

	if (errors > 0) {
		Serial << "Record content" << ENDL;
		print(Serial);
	}
	return (errors == 0);
}

