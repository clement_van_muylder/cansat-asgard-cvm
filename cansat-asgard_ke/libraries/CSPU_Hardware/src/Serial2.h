/*
 * Serial2.h
 * 
 * This file must be included to define the Serial2 object when it is not defined in the core
 * for a particular board (If Serial2 is defined, including this file will not modify it).
 * 
 * Currently supports only boards with ATSAMD21G18 controller 
 */

#if defined(UBRR2H) || defined(HAVE_HWSERIAL2)
    // If any of the above is defined, Serial2 global object is available, 
    // Nothing needs to be done.
constexpr const char* Serial2_PinLabels="TX??, RX=?? (complete in Serial2.h)";

#elif defined(ARDUINO_ARCH_SAMD)

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
 
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
constexpr const char* Serial2_PinLabels="TX=10, RX=11";
#elif defined(ARDUINO_ITSYBITSY_M4)
constexpr const char* Serial2_PinLabels="TX=A4, RX=A3";
#else
constexpr const char* Serial2_PinLabels="TX??, RX=?? (complete in Serial2.h)";
#endif

/** @ingroup CSPU_Hardware
 *  @brief This class encapsulates the configuration and use of Sercom module 1 to manage an additional 
 *  serial port  (Serial2) on µControllers with Sercom (Serial communication) modules. It is 
 *  developed for the ATSAMD21G18 controller used by Adafruit FeatherM0 Express board (although it
 *  should work for any board based on the same controller. </br>
 *
 *  This class is expected to be instantiated only once, as object Serial2, which is declared file Serial.h.
 *  See usage documentation of @ref Serial2.
 *  @par Pin allocation:
 *   - SAMD21: TX is on D10 and RX on D11.
 *   - SAMD51: TX is on pin 18 (on ItsyBitsy M4 : A4) and RX on pin 17 (on ItsyBitsy M4 : A3).
 *
 *  NB: Serial2 is a subclass of Uart, HardwareSerial and Stream. 
 *  
 *  @par Important implementation notes:
 *   - By default, variants define (in variant.h) a number of symbols:
 *   	 - SERIAL_PORT_HARDWARE_OPEN to identify a serial port which is open for use
 *   	   (its RX & TX pins are NOT connected to anything by default).
 *       - SERIAL_PORT_HARDWARE. For Adafruit Feather M0 Express and ItsyBitsy M0/M4 Express,
 *         SERIAL_PORT_HARDWARE is Serial1.
 *       - SERIAL_PORT_MONITOR and SERIAL_PORT_USBVIRTUAL which for Adafruit Feather M0
 *         Express and ItsyBitsy M0/M4 Express are both Serial.
 *     Serial and Serial1 ARE distinct, and provide 2 serial ports out of the box.
 *   - Some Sercoms are configured by default and should usually not be modified: 
 *     The µC ATSAMD21G18 (and hopefully all other SAMD21 µC) has 5 sercom modules:
 *     0 is reserved for the USB serial, on D0 and D1
 *     1 is free and can be used with pins D10, D11, D12 and D13.
 *     2 is free and can be used with pins D0 to D5 and D22. Since D0-D1 are used by
 *     	 SercomOn abd D22 by Sercom4, it leaves us with pins D2 thru D5.
 *     3 is used for the I2C bus,
 *     4 is used for the SPI bus,
 *     5 is used for Programming/Debug port (if any (e.g on Arduino Zero), not on FeatherM0).
 *     For SAMD51 µC, <https://forum.arduino.cc/t/samd51-multiple-serial-solved/659368/4>
 *     seems to says that sercom 3 is free for Serial2.
 *   - Not all sercoms can be configured on all pins, due to limitations in the 
 *     multiplexers (or in the configuration files).
 *     For SAMD21, this class supports the use of Sercom1, with TX on D10, RX on D11.
 *     For SAMD51, this class supports the use of Sercom4, with TX on 16 (A2), RX on 17 (A3)
 *   - A similar attempt to a Serial3 class exists for SAMD21, Sercom 2, with TX on D4,
 *     RX on D3 but is not operational (see documentation in 24a0/SW Architecture).
 *  @par References:
 *  	- <https://learn.adafruit.com/using-atsamd21-sercom-to-add-more-spi-i2c-serial-ports/creating-a-new-serial>
 *  	      (which also covers SAMD51, despite its title). Very informative document!
 * 	    - <https://forum.arduino.cc/t/samd51-multiple-serial-solved/659368/4>
 * 	    - SAMD21 datasheet (see project folder 2113)
 * 	    - SAMD51 datasheet (see project folder 2114)
 *
 */
class SercomSerial1 : public Uart {
 public:
    SercomSerial1();
    /** Initialize serial port */
    void begin(unsigned long baudRate);
    /** Obtain the TX pin number */
    byte getTX() { return tx;};
    /** Obtain the RX pin number */
    byte getRX() { return rx;};
 private:
    byte rx;
    byte tx;
 };

/** @ingroup CSPU_CansatAsgard
 *  @brief The predefined object to use to use a second hardware serial port with TX is on D10 and RX on D11. 
 *  @par Usage
 *  @code
 *  #include "Serial2.h"
 *  
 *  //... in your code ...
 *  Serial2.begin(115200);
 *  Serial2.println("xxxx");
 *  if (Serial2.available()) {
 *    char c = Serial2.read();
 *    // ....
 *  }
 *  @endcode
 *  For details see @ref SercomSerial1.  */
extern SercomSerial1 Serial2;
#  ifndef HAVE_HWSERIAL0
#    define HAVE_HWSERIAL0 1
#  endif
#  ifndef HAVE_HWSERIAL1
#    define HAVE_HWSERIAL1 1
#  endif
#  ifndef HAVE_HWSERIAL2
#    define HAVE_HWSERIAL2 1
#  endif
#  ifndef UBRR2H
#    define UBRR2H
#  endif
#else
#error "Serial2.h: currently only supports board with a second UART or using SAMD21/SAMD51 processor"
#endif
