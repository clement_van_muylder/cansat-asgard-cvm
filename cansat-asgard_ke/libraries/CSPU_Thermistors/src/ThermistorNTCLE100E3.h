/*
   ThermistorNTCLE100E3.h
*/

#pragma  once
#include "ThermistorSteinhartHart.h"

/**
 * @ingroup CSPU_Thermistors
 * @brief This a subclass of ThermistorSteinhartHart customized for thermistor NTCLE100E3223*BO,123*BO,153*BO.
 * Use this class to read thermistor resistance and convert to degrees.
 * Wiring: VCC to thermistor, thermistor to serialresistor, serialresistor to ground.

 */
class ThermistorNTCLE100E3: public ThermistorSteinhartHart {
  public:
  /**
   * @param theVcc  The voltage supplied to the serial resistor+thermistor assembly
   * @param theAnalogPinNbr this is the pin of the card in which we put the cable to read the resistance.
   * @param theSerialResistor The serial resistor (ohms).
   */
    ThermistorNTCLE100E3(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
             ThermistorSteinhartHart(theVcc, theAnalogPinNbr, 22000.0, 3.354016E-03,  2.744032E-04, 3.666944E-06, 1.375492E-07, theSerialResistor) {};

};
