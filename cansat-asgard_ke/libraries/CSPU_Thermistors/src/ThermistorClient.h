/*
   ThermistorClient.h
*/

#pragma once
#include "Thermistor.h"
#include "CansatRecord.h"

/** @ingroup CSPU_Thermistors
    @brief This class provides a convenient interface to access up to 3 thermistors and
           store readings in a CansatRecord. The number of thermistors is controlled by
           preprocessor symbols INCLUDE_THERMISTOR2 and INCLUDE_THERMISTOR3, defined in
           CansatRecord.h.
*/

class ThermistorClient {
  public:
	/** Constructor
	 *  @param therm1 A pointer to a first thermistor object, fully configured. This
	 *  	   		  pointer may be NULL. Ownership of the object is not transfered: this
	 *  	   		  object will never delete the object.
	 *  @param therm2 A pointer to a second thermistor object, fully configured. This
	 *  	   		  pointer may be NULL. Ownership of the object is not transfered: this
	 *  	   		  object will never delete the provided object.
	 *  @param therm3 A pointer to a third thermistor object, fully configured. This
	 *  	   		  pointer may be NULL. Ownership of the object is not transfered: this
	 *  	   		  object will never delete the provided object.
	 */
    ThermistorClient(Thermistor *therm1=NULL, Thermistor *therm2=NULL, Thermistor *therm3=NULL);

	/**
	 *  Configure the thermistor objects to use.
	 *  @param therm1 A pointer to a first thermistor object, fully configured. This
	 *  	   		  pointer may be NULL. Ownership of the object is not transfered: this
	 *  	   		  object will never delete the object.
	 *  @param therm2 A pointer to a second thermistor object, fully configured. This
	 *  	   		  pointer may be NULL. Ownership of the object is not transfered: this
	 *  	   		  object will never delete the object.
	 *  @param therm3 A pointer to a third thermistor object, fully configured. This
	 *  	   		  pointer may be NULL. Ownership of the object is not transfered: this
	 *  	   		  object will never delete the object.
	 */
    void setThermistors(Thermistor *therm1, Thermistor *therm2=NULL, Thermistor *therm3=NULL);

    /** @brief  this read the temperature for each thermistor and write it in the provided CansatRecord
     *  @param record The record to be completed with the values of temperature.
     *  @return true if the reading is successful, false otherwise
     */
    bool readData(CansatRecord& record) const;

  private:
    Thermistor *thermistor1; /**< The object handling the first thermistor */
#ifdef INCLUDE_THERMISTOR2
    Thermistor *thermistor2; /**< The object handling the second thermistor  (optional) */
#endif
#ifdef INCLUDE_THERMISTOR3
    Thermistor *thermistor3; /**< The object handling the thired thermistor  (optional) */
#endif
};
