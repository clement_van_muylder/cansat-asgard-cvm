/*
   VariableSerialResistor.h
*/
#pragma once

#include "Arduino.h"
/**
 * @ingroup CSPU_Thermistors
 * @brief   This class calculates the value of a variable resistor by
 * 			measuring the tension at mid-point.
 * 			Wiring: from Vcc to a known resistor, to the variable resistor,
 *			to the ground.  Voltage is measured at the middle point using an
 *			analog pin from the µController.
 */
class VariableSerialResistor {
  public:
  /**
   * @param theVcc the source level (in volts)
   * @param theAnalogPinNbr this is the number of the analog pin
   * @param theKnownResistor this is the value of the VCC to mid-point resistor, in ohms.
   */
	VariableSerialResistor(	const float theVcc,
					const uint8_t theAnalogPinNbr,
					const float theKnownResistor);
    /**
     * @brief Read voltage (using the average of several readings) and derive the
     *        resistance of the variable resistor.
     * @return The resulting resistance in ohms.
     */
    float readResistance()const;

  private:
    float knownResistor;
    uint8_t analogPinNbr;
    float vcc;
};
