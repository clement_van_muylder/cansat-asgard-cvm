/*
    This program tests the ThermistorClient class with the following thermistors
     - NTCLE305E3503SB
     - NTCLE305E4502SB
     - Littelfuse GT103J1K
     and with the BMP, using a 3.3V regulator for the thermistors. 

    Wiring (VCC is the output of the voltage regulator, if any is used, or the
    output 3.3V or 5V of the board if not (in this case set useExternalGenerator
    to false):
      VCC - NTCLG300E3502SB - A1 - 10kOhm - GND
      Vcc - GT103J1K - A0 - 10kohms - GND
      VCC - ARF

      µC    BMP280
      3V    Vin
      GND   GND
      SCL   SCK (with pull-up resistor to Vin)
      SDA   SDI (with pull-up resistor to Vin)

      µC   Regulator
      GND   GND
      USB   IN
            OUT = VCC
            EN to IN
     Usage: use csvFormat constant to select between human-readable output and csv output (for use in Excel sheet);

     and measurement of the tension 
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr bool csvFormat = true;// Set to true to generate CSV formatted output.
constexpr bool useExternalRef = false; // Set to true if a external regulator is used.

// (Constants describing wiring and thermistor models are now in there classes).

#define DEBUG
#include "DebugCSPU.h"
#include "ThermistorSteinhartHart.h"

#include "ThermistorNTCLE300E3502SB.h"
#include "BMP_Client.h"
#include "ThermistorGT103J1K.h"

#ifdef ARDUINO_AVR_UNO
constexpr float Vcc=5.0;
static constexpr uint16_t Nsteps = 1023;
#elif defined(ARDUINO_ARCH_SAMD)
constexpr float Vcc=3.3;
static constexpr uint16_t Nsteps = 4095;
#else
#error "Board not recognised"
#endif

ThermistorNTCLE300E3502SB NTCLE300E3502SB(Vcc, A1, 10000.0);
ThermistorGT103J1K GT103J1K(Vcc, A0, 10000.0);

CansatRecord record;
constexpr float mySeaLevelPressure = 1034.6;
BMP_Client bmp;

void setup() {
  // to know wich Vmax and Nsteps we have to use;

  DINIT(115200);
  Serial << "Testing Thermistor model classes for the G-Mini thermistors and the BMP280 or 388" << ENDL;
  Serial << "  Vcc = "<< Vcc << "V" <<ENDL;
  Serial << "  ADC steps: " << Nsteps << ENDL;
  
  Wire.begin();
  if (!bmp.begin(mySeaLevelPressure)) {
    Serial.print("Could not find a valid BMP sensor, check wiring!");
    exit(-1);
  }
  if(useExternalRef)
  {
	  analogReference(AR_EXTERNAL); // AR_EXTERNAL, no external for Feather M0 Express
	  Serial << "Set reference tension to AR_EXTERNAL" << ENDL;
  }
  else
  {
	  Serial << "Not using external reference"<< ENDL;
  }
  if (csvFormat) {
    Serial << "time, ref tension, A0 ADC value, A0 voltage, NTCLE300E3502SB, GT103J1K, BMP" << ENDL;
  }
}

void loop() {
  float temp_300E350 = NTCLE300E3502SB.readTemperature();
  float temp_GT = GT103J1K.readTemperature();
  //float res = GT103J1K.readResistance();
  int A0_SensorValue = analogRead(A0);
  float V = A0_SensorValue * (Vcc / Nsteps);

  if (csvFormat) {
	  Serial <<  millis() << ", " << A0_SensorValue <<  ", " << V << temp_300E350 << ", " << temp_GT ;
	  bool result = bmp.readData(record);
	  if (result == true) {
		  Serial << ", " << record.temperatureBMP << ENDL;
	  } else {
		  Serial << ",0" <<ENDL;
	  }
  } else {
	  Serial <<  millis() << ", A0 ADC=" << A0_SensorValue << ", A0 tension: " << V << "°C, NTCLE300E3502SB: " << temp_300E350 << "°C, GT" << temp_GT << "°C";
	  bool result = bmp.readData(record);
	  if (result == true) {
		  Serial << ", temp BMP: " << record.temperatureBMP << "°C, pressure:" << record.pressure << " hPa, " << "altitude:" << record.altitude << " m" << ENDL;
	  } else {
		  Serial << "0 " << ENDL;
	  }
  }
  delay(1000);
}
