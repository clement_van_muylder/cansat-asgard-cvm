/*
 * Latch.cpp
 */

#include "CansatConfig.h"
#include "DebugCSPU.h"
#include "Latch.h"

#define DBG_DIAGNOSTIC 1

void Latch::neutral() {
	DPRINTSLN(DBG_DIAGNOSTIC,"Latch::neutral called. Implement in subclass");
}


