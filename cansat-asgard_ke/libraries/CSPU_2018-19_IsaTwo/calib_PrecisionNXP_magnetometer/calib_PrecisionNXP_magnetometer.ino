#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_FXOS8700.h"

//LSM9DS0 compass; //Copy the folder "HMC5883L" in the folder "C:\Program Files\Arduino\libraries" and restart the arduino IDE.
Adafruit_FXOS8700 accelmag = Adafruit_FXOS8700(0x8700A, 0x8700B);
float xv, yv, zv;

void setup()
{
  Serial.begin(9600);
  Wire.begin();
  if(!accelmag.begin(ACCEL_RANGE_4G))
  {
    /* There was a problem detecting the Precision NXP board ... check your connections */
    Serial.print(F("Ooops, no Precision NXP board detected ... Check your wiring!"));
    while (1);
  }
}

void loop()
{
  sensors_event_t aevent, mevent;

  /* Get a new sensor event. Results are in µTesla */
  accelmag.getEvent(&aevent, &mevent);
  xv = (float)mevent.magnetic.x;
  yv = (float)mevent.magnetic.y;
  zv = (float)mevent.magnetic.z;

  Serial.flush();
  Serial.print(xv);
  Serial.print(",");
  Serial.print(yv);
  Serial.print(",");
  Serial.print(zv);
  Serial.println();

  delay(100);
}
