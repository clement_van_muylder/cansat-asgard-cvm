/*
  createTestFiles.ino
*/
#define DEBUG_CSPU 
#include "DebugCSPU.h"
#include "SdFat.h"
#include "SPI.h"
#include "IsaTwoRecord.h"
#include "StringStream.h"

const byte SD_CS = 13;
SdFat sd;

void createRecordFile(const char* name, unsigned int numRecords)
{
  SdFile file;
  file.open(name, O_CREAT | O_WRITE);     
  IsaTwoRecord rec;
  rec.timestamp=1000000;
  Serial << "Creating large file... be patient..." << ENDL;
  for (unsigned int i=0;i<numRecords;i++ ) {
       rec.timestamp+=100;
       rec.co2+=0.001;
       String CSV_Version;
       CSV_Version.reserve(200);
       StringStream sstr(CSV_Version);
       rec.printCSV(sstr);
       file.println(CSV_Version);
  }
  file.close();                                                      
  Serial << "Created file '" << name << "' with " << numRecords << " records." << ENDL;
}

void setup() {
  SdFile file;
  
  DINIT(115200);
  SPI.begin();
  Serial << "Initializing SD (CS=" << SD_CS << ")" << ENDL;
  if (!sd.begin(SD_CS)) {
    sd.initErrorHalt();
  } else {
    Serial << "SD ok" << ENDL;
  }
  if (sd.exists("Folder1")|| sd.exists("Folder1/file1.txt")|| sd.exists("Folder1/file2.txt")|| sd.exists("Folder2")|| sd.exists("Folder2/file3.txt")|| sd.exists("Folder2/file4.txt")|| sd.exists("/file5.txt")) {
    Serial << F("Please remove existing Folder1, Folder2, file1.txt, file2.txt, file3.txt, file4.txt and file5.txt") << ENDL;
    Serial << "Current content:" << ENDL;
    sd.ls("/", LS_R | LS_SIZE); 
    while(1) delay(100);
  }

  sd.vwd()->rewind();

  sd.mkdir("Folder1");                                                      //Creates a folder in the root direcotry called "Folder1"
  file.open("/Folder1/file1.txt", O_CREAT | O_WRITE);                       //Creates a '.txt' file in Folder1 called file1
  file.println("TEST 1, 2, 3 . . . File: /Folder1/file1.txt");              //Writes to file1.txt
  file.println("Test . . . test . . . TEST . . .");                         //Writes to file1.txt
  file.println("Special Character test: / : ; . , ? % * & ! ");             //Writes to file1.txt
  file.close();                                                             //Closes file1.txt to allow further file operation
  file.open("/Folder1/file2.txt", O_CREAT | O_WRITE);                       //Creates a '.txt' file in Folder1 called file2
  file.println("TEST 4, 5, 6 . . . File: /Folder1/file1.txt");              //Writes to file2.txt
  file.println("Test2 . . . test2 . . . TEST2 . . .");                      //Writes to file2.txt
  file.println("Special Character test 2: / : ; . , ? % * & ! ");           //Writes to file2.txt
  file.close();                                                             //Closes file2.txt to allow further file operation
  Serial << "Created Folder 1 and Test files in it" << ENDL;

  sd.mkdir("/Folder2");                                                     //Creates a folder in the root direcotry called "Folder2"
  file.open("/Folder2/file3.txt", O_CREAT | O_WRITE);                       //Creates a '.txt' file in Folder2 called file3
  file.println("TEST 7, 8, 9 . . . File: /Folder2/file3.txt");              //Writes to file3.txt
  file.println("Test3 . . . test3 . . . TEST3 . . .");                      //Writes to file3.txt
  file.println("Special Character test3: / : ; . , ? % * & ! ");            //Writes to file3.txt
  file.close();                                                             //Closes file4.txt to allow further file operation
  file.open("/Folder2/file4.txt", O_CREAT | O_WRITE);                       //Creates a '.txt' file in Folder2 called file4
  file.println("TEST 10, 11, 12 . . . File: /Folder2/file4.txt");           //Writes to file4.txt
  file.println("Test4 . . . test4 . . . TEST4 . . .");                      //Writes to file4.txt
  file.println("Special Character test4: / : ; . , ? % * & ! ");            //Writes to file4.txt
  file.close();                                                             //Closes file4.txt to allow further file operation
  Serial << "Created Folder 2 and Test files in it" << ENDL;

  file.open("/file5.txt", O_CREAT | O_WRITE);                               //Creates a '.txt' file in the root directory called file5
  file.println("TEST 13, 14, 15 . . . File: /file5.txt");                   //Writes to file5.txt
  file.println("Test5 . . . test5 . . . TEST5 . . .");                      //Writes to file5.txt
  file.println("Special Character test5: / : ; . , ? % * & ! ");            //Writes to file5.txt
  file.close();                                                             //Closes file5.txt to allow further file operation
  Serial << "Created test5.txt, test file in root directory " << ENDL;

  createRecordFile("/Folder1/rec1000.txt",1000);
  createRecordFile("/Folder2/rec10k.txt",10000);
  sd.ls("/", LS_R | LS_SIZE);                                                         //Lists the root directory and subdirectories for a human verificaion
}

void loop() {}
