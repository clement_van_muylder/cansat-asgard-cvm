/*
    Test program for the use of the XBee API mode through streaming operators,
    + binary transfer of IsaTwoRecords.
*/

#define RF_ACTIVATE_API_MODE  // If undefined, transparent mode is used. 
#include "IsaTwoXBeeClient.h"
#include "elapsedMillis.h"
#include "IsaTwoHW_Scanner.h" // For OPEN_RF_STRING/CLOSE_RF_STRING

constexpr uint8_t StringBufferSize = 150;
constexpr unsigned int MaxNumberOfErrors = 10; // When this number of error is reached, the test stops.
constexpr bool DisplayOutgoingFrame = false;
constexpr bool DisplayIncomingFrame = false;
constexpr bool DocumentEmission = false;

#define RF-TSCV  // Define or undefine according to the XBee module connected.
#undef A //Define for pair A or undefine for pair B

#ifdef RF-TSCV
constexpr unsigned long EmissionPeriod = 1000; // msec. The emission period
# ifdef A
IsaTwoXBeeClient xb(0x0013a200, 0x418fb90a); // Address of Can_A Xbee to load in RF-TSCV_A one.
# else //Pair B
IsaTwoXBeeClient xb(0x0013a200, 0x418fbbeb); // Address of Can_B Xbee to load in RF-TSCV_B one.
# endif
#else
constexpr unsigned long EmissionPeriod = 70; // msec. The emission period
# ifdef A 
IsaTwoXBeeClient xb(0x0013a200, 0x41827f67); // Address of RF-TSCV_A Xbee to load in the Can_A one.
# else //Pair B
IsaTwoXBeeClient xb(0x0013a200, 0x418fc78b); // Address of RF-TSCV_B Xbee to load in the Can_B one.
# endif
#endif
IsaTwoXBeeClient* xbPtr = &xb;

int currentTest = 0;
elapsedMillis elapsed;
unsigned int numErrors = 0;

void setup() {
  DINIT(115200);
  Serial1.begin(115200);
  xb.begin(Serial1);

  Serial << "Set up OK" << ENDL;
}
const char * t0_TestString = "0_A test string, not too long";
const char * t1_TestString = "1_Test: byte=5, char=a, int=5, uint8=5, int8=-5, uint16=5, int16=-5";

void sendSomething() {
  switch (currentTest) {
    case 0:
      if (DocumentEmission) {
        Serial << ENDL << "A. Sending string '" << t0_TestString << "'" << ENDL;
      }
      RF_OPEN_STRING(xbPtr);
      xb << t0_TestString << ENDL;
      RF_CLOSE_STRING(xbPtr);
      break;
    case 1:
      if (DocumentEmission) {
        Serial << ENDL << "B. Sending string '" << t1_TestString << "'" << ENDL;
      }
      RF_OPEN_STRING(xbPtr);
      xb << "1_Test: byte=" << (byte) 5 << ", char=" << 'a'
         << ", int=" << (int) 5
         << ", uint8=" << (uint8_t) 5
         << ", int8=" << (int8_t) - 5
         << ", uint16=" << (uint16_t) 5
         << ", int16=" << (int16_t) - 5;
      RF_CLOSE_STRING(xbPtr);
      break;

    case 2:
      if (DocumentEmission) {
        Serial << ENDL <<  "C. Sending an IsaTwoRecord" << ENDL;
      }
      IsaTwoRecord rec;
      rec.timestamp = 12345;
      rec.accelRaw[0] = 3;  rec.accelRaw[1] = 4;  rec.accelRaw[2] = 5;
      rec.GPS_LongitudeDegrees = 98.765;
      rec.GPS_LatitudeDegrees = 34.56789;
      rec.pressure = 1013.7;
      rec.co2 = 8765;

      if (DisplayOutgoingFrame) {
        Serial << "Record content: " << ENDL;
        rec.print(Serial);
      }
      xb.send(rec);
      break;
  }
  currentTest++;
  currentTest = currentTest % 3;
  if (!DocumentEmission) {
    Serial << '.' ;
  }
}

bool processString(uint8_t* payloadPtr, uint8_t payloadSize) {
  char buffer[StringBufferSize];
  bool ok = false;
  String refString;
  if (payloadSize > StringBufferSize + 1) {
    Serial << "*** Error: payload exceeds buffer size: ignored" << ENDL;
    numErrors++;
  }
  if (xb.getString(buffer, payloadPtr, payloadSize)) {
    //Serial << "Received string: '" << buffer << "'" << ENDL;
    switch (buffer[0]) {
      case '0':
        refString = t0_TestString;
        refString += '\n';
        if (strcmp(buffer, refString.c_str()) != 0) {
          Serial << "*** Error: expected '" << refString << "'" << ENDL;
          Serial << "***        got '" << buffer << "'" << ENDL;
          numErrors++;
        } else ok = true;
        break;
      case '1':
        refString = t1_TestString;
        if (strcmp(buffer, refString.c_str()) != 0) {
          Serial << "*** Error: expected '" << refString << "'" << ENDL;
          Serial << "***        got '" << buffer << "'" << ENDL;
          numErrors++;
        } else ok = true;
        break;
      default:
        Serial << "*** Error: unexpected string '" << buffer << "'" << ENDL;
        numErrors++;
    }// switch
  } else {
    Serial << "*** Error extracting string" << ENDL;
    numErrors++;
  } // if
  return ok;
}

bool processDataRecord(uint8_t* payloadPtr, uint8_t payLoadSize) {
  IsaTwoRecord rec;
  bool ok = false;
  if (xb.getDataRecord(rec, payloadPtr, payLoadSize)) {
    if (	(rec.timestamp   != 12345) ||
          (rec.accelRaw[0] != 3) ||
          (rec.accelRaw[1] != 4) ||
          (rec.accelRaw[2] != 5) ||
               (rec.co2 != 8765)) {
      Serial << "*** Error in extracted record (int)." << ENDL;
      rec.print(Serial);
      numErrors++;
    } else if (( fabs(rec.GPS_LongitudeDegrees - 98.765) > 0.001) ||
               (fabs(rec.GPS_LatitudeDegrees  - 34.56789) > 0.001) ||
               (fabs(rec.pressure - 1013.7) > 0.001) 
              )
    {
      Serial << "*** Error in extracted record (float)." << ENDL;
      Serial << "co2=" << rec.co2 << ENDL;
      rec.print(Serial);
      numErrors++;
    } else {
      ok = true;
    }
  } else {
    Serial << "*** Error extracting record" << ENDL;
    numErrors++;
  } // if
  return ok;
}

void receiveMessage() {
  uint8_t* payloadPtr;
  uint8_t payloadSize;
  bool ok = false;
  static int okCounter = 0;
  if (xb.receive(payloadPtr, payloadSize)) {
    if (payloadSize == 0) {
      Serial << "*** Error: payloadSize=0 " << ENDL;
      numErrors++;
    } else {
      uint8_t recordType = payloadPtr[0];
      switch (recordType) {
        case (uint8_t) IsaTwoRecordType::StatusMsg:
        case (uint8_t) IsaTwoRecordType::CmdRequest:
        case (uint8_t) IsaTwoRecordType::CmdResponse:
          ok = processString(payloadPtr, payloadSize);
          break;
        case (uint8_t) IsaTwoRecordType::DataRecord:
          ok = processDataRecord(payloadPtr, payloadSize);
          break;
        default:
          Serial << "*** Error: unexpected record type: " << recordType << ENDL;
          numErrors++;
      }
      if (DisplayIncomingFrame) {
        Serial << ENDL << ENDL;
        Serial << "Received frame:" << ENDL;
        xb.printFrame(payloadPtr, payloadSize);
      }
    }
  }
  if (ok) {
    Serial << " ok " << ENDL;
    okCounter++;
    okCounter = okCounter % 10;
    if (okCounter == 0) Serial << ENDL;
  } else okCounter = 0;
}

void loop() {

  if (numErrors >= MaxNumberOfErrors) {
    delay(100);
    return;
  }

  if (elapsed >= EmissionPeriod) {
    sendSomething();
    elapsed = 0;
  }

  receiveMessage();

  if (numErrors >= MaxNumberOfErrors) {
    Serial << "*** Max number of errors reached ("
           << MaxNumberOfErrors << "). Test over. " << ENDL;
  }

}
