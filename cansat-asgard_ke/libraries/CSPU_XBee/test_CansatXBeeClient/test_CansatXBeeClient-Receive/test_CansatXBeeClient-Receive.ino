/**
  Test for the receiving part of CansatXBeeClient.h (using the ground-side XBee module)
  Test with Feather M0 board.

  Wiring      : see test_XBeeClient-Send sketch (but using the ground-side XBee module)
  Instructions: see test_CansatXBeeClient-send sketch

*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#  error "This program only works on Feather MO_Express"
#endif

#include "CansatConfig.h"
#include "CansatXBeeClient.h"
#include "CansatRecordExample.h"
#include <type_traits>

constexpr bool AbortIfUsingWrongXBeeModule=false; // If true, the test is aborted in case the XBee module is not the expected one.
#define DBG 1

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
CansatXBeeClient xbc(CanXBeeAddressSH, CanXBeeAddressSL); // Defined in CansatConfig.h
CansatRecordExample myRecord;
char myString[xbc.MaxStringSize + 10]; // AAAA
CansatFrameType stringType;
bool isRecord;

void setup() {
  DINIT(115200);

  digitalWrite(LED_BUILTIN, HIGH);
  Serial << "***** RECEIVER SKETCH (testing CansatXBeeClient) *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value
  Serial << "Initialization over. " << ENDL;
  Serial << "Test record size = " << sizeof(myRecord) << " bytes, base class size = "
         << sizeof(CansatRecord) << " bytes." << ENDL;
  Serial << "Test record address: " << (uint32_t) &myRecord << ENDL;

  myRecord.print(Serial);

  Serial << "Configuration of XBee module (assumed to be the ground side of set " << RF_XBEE_MODULES_SET << ")" << ENDL;
  xbc.printConfigurationSummary(Serial);
  auto xbeeRole = xbc.getXBeeSystemComponent();
  if (xbeeRole != CansatXBeeClient::CansatComponent::RF_Transceiver) {
    Serial << "Warning: this XBee module is '" << xbc.getLabel(xbeeRole) << "' while it should be '"
           << xbc.getLabel(CansatXBeeClient::CansatComponent::RF_Transceiver) << "'." << ENDL;
    Serial << "Check the content of Cansat config (RF_XBEE_SET, address of XBee) and connected module." << ENDL;
    if (AbortIfUsingWrongXBeeModule) {
      Serial << "**** ABORTED ***" << ENDL;
      Serial.flush();
      exit(-1);
    } else Serial << "**** Performing test anyway ***" << ENDL;
  }
  delay(1000);
  Serial << "Initialisation over." << ENDL;
}

void loop() {

  static uint32_t counter = 0;
  counter++;

  myRecord.altitude = -5;
  myRecord.integerData = 222;
  uint8_t seqNbr;
  if (xbc.receive(myRecord, myString, stringType, seqNbr, isRecord)) {
    counter = 0;
    if (isRecord) {
      Serial << "Received a record. ";
      if (myRecord.checkValues()) {
        Serial << " content is ok!" << ENDL;
      } else {
        Serial << " *** Content NOT ok: " << ENDL;
        myRecord.print(Serial);
      }
    } else {
      Serial << "Received a string, with type=" << (int) stringType
             << " and sequence number = " << seqNbr << ENDL;
      Serial << "'" << myString << "'" << ENDL;
      if (stringType != CansatRecordExample::TestStringType) {
        Serial << "*** Error: string type should be "
               << (int) CansatRecordExample::TestStringType << ENDL;
      }
      if (seqNbr != CansatRecordExample::SequenceNumber) {
        Serial << "*** Error: string sequence number should be "
               << CansatRecordExample::SequenceNumber << " (got " << seqNbr << ")" << ENDL;
      }

      if (strcmp(myString, CansatRecordExample::testString)) {
        Serial << "*** Error: " << ENDL;
        Serial << "  Should be: '" << CansatRecordExample::testString << ENDL;
      }
    }
  }
  if (counter > 600000) {
    counter = 0;
    Serial << "Still alive (but not receiving anything)..." << ENDL;
  }
}
