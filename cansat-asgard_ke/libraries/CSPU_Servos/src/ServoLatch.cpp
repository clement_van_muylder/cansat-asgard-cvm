/*
 * ServoLatch.cpp
 */
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
    /* v1.1.8 of the Servo library does not support the SAMD51 processor. See note in class comment */

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOCK 0
#define DBG_UNLOCK 0
#define DBG_BEGIN 0
#define DBG_ENABLE 1
#include "ServoLatch.h"

static constexpr bool AttachDetachEveryTime=true;
static constexpr unsigned long ServoMvtDelay=500; // Delay in msec for servo move.

bool ServoLatch::begin( const uint8_t PWM_Pin,  uint8_t enablePin, int minPulseWidth, int maxPulseWidth) {
  thePWM_Pin = PWM_Pin;
  theMinPulseWidth = minPulseWidth;
  theMaxPulseWidth = maxPulseWidth;
  theStatus=ServoLatch::LatchStatus::Undefined;
  DPRINTS(DBG_BEGIN, "minPulseWidth=");
  DPRINTLN(DBG_BEGIN, minPulseWidth );
  DPRINTS(DBG_BEGIN, "maxPulseWidth=");
  DPRINTLN(DBG_BEGIN, maxPulseWidth );

  theEnablePin=enablePin;
  if (enablePin != 0) {
	  pinMode(enablePin, OUTPUT);
  }
  if (!AttachDetachEveryTime) { 
	enableServo(true);
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth );
    DPRINTLN(DBG_BEGIN, "Attaching");
  }
  return true;
}

void ServoLatch::enableServo(bool status) {
	if (!theEnablePin) return;
	DPRINTS(DBG_ENABLE, "Enable: ");
	DPRINTLN(DBG_ENABLE, status);
	digitalWrite(theEnablePin, (status ? HIGH : LOW));
}

void ServoLatch::unlock() {
  DPRINTS(DBG_UNLOCK, "ServoLatch::unlock, using pin #");
  DPRINTLN(DBG_UNLOCK, thePWM_Pin);
  if (AttachDetachEveryTime) {
	enableServo(true);
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth );
    DPRINTLN(DBG_UNLOCK, "Enabling and attaching");
    delay(DelayAfterAttach);
    DPRINTS(DBG_UNLOCK, "Waiting=");
    DPRINTLN(DBG_UNLOCK, DelayAfterAttach);
  }
  myServo.write(unlockPosition);
  DPRINTS(DBG_UNLOCK, "Writing=");
  DPRINTLN(DBG_UNLOCK, unlockPosition);

  // Wait until the Servo actually moved
  delay(ServoMvtDelay);

  if (AttachDetachEveryTime) {
    delay(DelayBeforeDetach);
    DPRINTS(DBG_UNLOCK, "Waiting=");
    DPRINTLN(DBG_UNLOCK, DelayBeforeDetach);
    myServo.detach();
    enableServo(false);
    DPRINTLN(DBG_UNLOCK, "Detaching and disabling");
  }
  theStatus = LatchStatus::Unlocked;
}

void ServoLatch::lock() {
  DPRINTS(DBG_LOCK, "ServoLatch::lock, using pin #");
  DPRINTLN(DBG_LOCK, thePWM_Pin);
  if (AttachDetachEveryTime) {
	enableServo(true);
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth);
    DPRINTLN(DBG_LOCK, "Enabling  and Attaching");
    delay(DelayAfterAttach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayAfterAttach);
  }
  myServo.write(lockPosition);
  DPRINTS(DBG_LOCK, "Writing=");
  DPRINTLN(DBG_LOCK, lockPosition);
  // Wait until the Servo actually moved
  delay(ServoMvtDelay);
  
  if (AttachDetachEveryTime) {
    delay(DelayBeforeDetach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayBeforeDetach);
    myServo.detach();
    enableServo(false);
    DPRINTLN(DBG_LOCK, "Detaching and disabling");
  }
  theStatus = LatchStatus::Locked;
}

void ServoLatch::neutral() {
  DPRINTS(DBG_LOCK, "ServoLatch::neutral, using pin #");
  DPRINTLN(DBG_LOCK, thePWM_Pin);
  if (AttachDetachEveryTime) {
	enableServo(true);
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth  );
    DPRINTLN(DBG_LOCK, "Enabling and Attaching");
    delay(DelayAfterAttach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayAfterAttach);
  }
  myServo.write((unlockPosition + lockPosition) / 2);
  DPRINTS(DBG_LOCK, "Writing=");
  DPRINTLN(DBG_LOCK, (unlockPosition + lockPosition) / 2);
  // Wait until the Servo actually moved
  delay(ServoMvtDelay);
  if (AttachDetachEveryTime) {
    delay(DelayBeforeDetach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayBeforeDetach);
    myServo.detach();
    enableServo(false);
    DPRINTLN(DBG_LOCK, "Detaching and disabling");
  }
  theStatus = LatchStatus::Neutral;
}

void ServoLatch::configure(int neutralPosition,int amplitude){
  unlockPosition=neutralPosition-(amplitude/2);
  lockPosition=neutralPosition+(amplitude/2);
}
#endif // supported on current board.
