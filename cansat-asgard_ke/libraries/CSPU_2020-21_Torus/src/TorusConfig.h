/*
	Specific configuration for the Torus project
*/

#pragma once
#include "CansatConfig.h"
#include "ServoWinch.h"

// ********************************************************************
// ***************** A. Servo-winch parameters ************************
// ********************************************************************

constexpr uint8_t ServoWinch_mmPerMove = 10;		// Number of mm to move each time the winch position is updated (once every AsynchServoWinch::updateFrequency)
constexpr byte ServoWinchPWM_PinNbr = 9;		// Number of PWM pin used to control the servo winch.

constexpr ServoWinch::ValueType ServoInitAndEndPositionType = ServoWinch::ValueType::ropeLength; /**< Type of servoInitAndEndPosition */
constexpr uint16_t ServoInitAndEndPosition = 180; /**< Position of the servo to be set at the beginning and at the end. */
constexpr uint16_t ServoOvershootWhenRetracting = 20; /**< By how much does the servo have to overshoot when retracting */
constexpr uint16_t ServoOvershootWhenExtending = 0;   /**< By how much does the servo have to overshoot when extending */

// ********************************************************************
// ************ B. reference altitude calculation parameters **********
// ********************************************************************
// NOTE: This section is now integrated in CansatConfig.h for reuse in next projects
//       (calculation of reference altitude is moved to BMP_Client).



// ********************************************************************
// ****************** C. Flight control parameters ********************
// ********************************************************************

constexpr uint32_t TorusMinDelayAfterTargetReached=3000;  // msec. The minimum duration to wait after servo reached target
														  // before setting another target.
constexpr uint32_t TorusMinDelayBetweenPhaseChange=500;   // msec. The minimum duration between 2 successive phase detection,
														  // to avoid that a couple of aberrant readings or high-frequency changes
														  // would cause oscillations between flight phases.
constexpr uint32_t TorusMinDelayBeforeFlightControl=10000;// msec The minimum delay between the startup of the processor
														  // and the startup of the flight control process. This delay does
														  // not apply to reference altitude calculation.
constexpr float TorusMinAltitude=100.0; 				  // m The altitude below which "ground conditions"
														  // (i.e before take-off or after landing) are assumed.
constexpr float TorusVelocityDescentLimit=2; 	 		  // m/s The minimum descent velocity from which the can is assumed
														  // to be descending.
constexpr float TorusVelocityLowerSafetyLimit=5; 		  // m/s The minimum descent velocity the can should always keep
											     	      // when it is descending (ie. descent velocity > TorusVelocityDescentLimit).
constexpr float TorusVelocityUpperSafetyLimit=16; 		  // m/s The maximum descent velocity the can should always keep,
												  	      // when it is descending (ie. descent velocity > TorusVelocityDescentLimit).

constexpr uint8_t TorusNumPhases=5; 					  // number of flight phases defined. Valid range: 1-14 (15=no phase).
/* Tables below define TorusNumPhases altitudes and rope lengths.
 * Altitudes must be decreasing from index 0
 * TorusPhaseRopeLen[0] is applicable when altitude is above TorusPhaseAltitude[0]
 * For 0 < i < TorusNumPhases:
 * TorusPhaseRopeLen[i] is applicable when altitude is above TorusPhaseAltitude[i]
 * and below altitude[i-1].
 * Below altitude TorusPhaseAltitude[TorusNumPhases-1], ground condition is assumed.
 * Example:
 *   TorusPhaseAltitude[TorusNumPhases]= { 800, 650, 500, 350, 200};
 *   TorusPhaseRopeLen[TorusNumPhases]= { 20, 30, 40, 50, 60};
 * Results in the following ropeLengh:
 * 		20 mm above (or at) 800 m,
 * 		30 mm in range ]800;650] m
 * 		40 mm in range ]650;500] m
 * 		50 mm in range ]500;350] m
 * 		60 mm in range ]350;200] m
 * 		Ground conditions strictly under 200m
 */
constexpr uint16_t TorusPhaseAltitude[TorusNumPhases]= { 656, 504, 365, 236, 115}; 
constexpr uint16_t TorusPhaseRopeLen[TorusNumPhases]= { 20, 50, 80, 110, 140};

