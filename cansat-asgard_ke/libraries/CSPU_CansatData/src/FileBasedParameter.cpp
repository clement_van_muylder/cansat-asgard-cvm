/*
 * FileBasedParameter.cpp
 */

#include "FileBasedParameter.h"

#define DBG_DIAGNOSTIC 1

#ifdef PARAM_FILE_INCLUDE_DELETE_METHODS

bool FileBasedParameter::deleteFile(SdFat& sd, const char* parameterName)
{
	DPRINTS(DBG_FILE_PARAM,"Deleting for param ");
	DPRINTLN(DBG_FILE_PARAM, parameterName);
	// Remember we return true if the file does not exist.
	bool result = false;
	if (!goToParamsDirectory(sd))  {
		DPRINTSLN(DBG_FILE_PARAM, "Cannot go to directory");
		return true;
	}
	char fileName[13]; // Standard 8.3 DOS name + \0
	getParamFileName(parameterName, fileName);
	if (sd.exists(fileName)) {
		DPRINTS(DBG_FILE_PARAM,"file exists, deleting ");
		DPRINTLN(DBG_FILE_PARAM, fileName);
		result=sd.remove(fileName);
	}
	else {
		result=true;
	}
	sd.chdir("/");
	return result;
};

bool FileBasedParameter::deleteAll(SdFat& sd)
{
	bool result = true;
	if (sd.exists(DirectoryPath)) {
		File dir=sd.open(DirectoryPath);
		if (dir.isDir()) {
			File f;
			while (result && f.openNext(&dir, O_WRITE)) {
#if (DBG_FILE_PARAM == 1)
				Serial << "Deleting ";
				f.printName(&Serial);
				Serial << ENDL;
#endif
				result = f.remove();
				DPRINTS(DBG_FILE_PARAM,"file remove result=");
				DPRINTLN(DBG_FILE_PARAM, result);
			}
			if (result) {
				result = sd.rmdir(DirectoryPath);
				DPRINTSLN(DBG_FILE_PARAM, "Removed directory");
				DPRINTS(DBG_FILE_PARAM,"remove result=");
				DPRINTLN(DBG_FILE_PARAM, result);
			}
		}
	}

	return result;
};
#endif

void FileBasedParameter::getParamFileName(const char* parameterName, char fileName[13]) {
	for (unsigned int i =0; i< 8;i++) {
		if (i <= (strlen(parameterName)-1)) {
			fileName[i]=parameterName[i];
		}else {
			fileName[i]='_';
		}
	}
	strcpy(fileName+8, ".txt");
	DPRINTS(DBG_FILE_PARAM, "fileName=");
	DPRINTLN(DBG_FILE_PARAM,fileName);
};

bool FileBasedParameter::openParamFile(SdFat& sd, const char* parameterName, File &f, bool write) {
	if (!goToParamsDirectory(sd)) return false;
	char fileName[13]; // Standard 8.3 DOS name + \0
	getParamFileName(parameterName, fileName);
	if (write) {
		// By default, content will overwirtten, but if the previous content was shorter that the
		// new content, the remaining will not be deleted. So, delete, and recreate.
		sd.remove(fileName);
		f= sd.open(fileName, O_WRITE | O_CREAT);
		DPRINTSLN(DBG_FILE_PARAM, "file open (write)");
	} else {
		f= sd.open(fileName, O_READ);
		f.rewind();
		DPRINTSLN(DBG_FILE_PARAM, "file open (read)");
	}
	sd.chdir("/");
	return (f.isOpen());
}


bool FileBasedParameter::goToParamsDirectory(SdFat& sd) {
	bool directoryExists=sd.chdir(DirectoryPath);
	if (!directoryExists) {
		if (sd.exists(DirectoryPath)) {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** Warning: path '/params' exists and is not a directory. Removing...");
			if (!sd.remove(DirectoryPath)) {
				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: Removing failed.");
				return false;
			}
		}
		if (!sd.mkdir(DirectoryPath)) {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot create /params folder");
			return false;
		}
	}
	return sd.chdir(DirectoryPath);
}




