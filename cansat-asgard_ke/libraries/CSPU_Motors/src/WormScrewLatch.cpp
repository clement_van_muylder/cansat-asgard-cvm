/*
 * WormScrewLatch.cpp
 */

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOCK 0
#define DBG_UNLOCK 0
#define DBG_BEGIN 0
#include "WormScrewLatch.h"

/* A ADAPTER
bool WormScrewLatch::begin( const uint8_t PWM_Pin,  uint8_t enablePin, int minPulseWidth, int maxPulseWidth) {
  thePWM_Pin = PWM_Pin;
  theMinPulseWidth = minPulseWidth;
  theMaxPulseWidth = maxPulseWidth;
  theStatus=ServoLatch::LatchStatus::Undefined;
  DPRINTS(DBG_BEGIN, "minPulseWidth=");
  DPRINTLN(DBG_BEGIN, minPulseWidth );
  DPRINTS(DBG_BEGIN, "maxPulseWidth=");
  DPRINTLN(DBG_BEGIN, maxPulseWidth );

  theEnablePin=enablePin;
  if (enablePin != 0) {
	  pinMode(enablePin, OUTPUT);
  }
  if (!AttachDetachEveryTime) { 
	enableServo(true);
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth );
    DPRINTLN(DBG_BEGIN, "Attaching");
  }
  return true;
}
*/


void WormScrewLatch::unlock() {
  // TO BE COMPLETED
  theStatus = LatchStatus::Unlocked;
}

void WormScrewLatch::lock() {
  //TO BE COMPLETED
  theStatus = LatchStatus::Locked;
}
