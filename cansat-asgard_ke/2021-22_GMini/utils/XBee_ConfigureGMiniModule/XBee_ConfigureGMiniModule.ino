/*
    Utility program to configure an XBee module for the G-Mini project

    Connect the XBee module and run the program. The appropirate configuration will be selected
    from the settings (in GMiniConfig.h)

     Wiring: µC to XBee module.
     Feather/ItsyBitsy
	   3.3V to VCC
	   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
	   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
	   GND  to GND
	 Uno: (RX-TX pins can be modified with constants below)
	   3.3V    to VCC
	   RX=D9   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
	   TX=D11  to DIN   CONFIRMED although it is the opposite to connect to XCTU!
	   GND     to GND

 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GMiniConfig.h"
#include "GMiniXBeeClient.h"
#include "CSPU_Test.h"

#ifdef ARDUINO_ARCH_SAMD
HardwareSerial &RF = Serial1;
#else
#   include "SoftwareSerial.h"
static constexpr int RF_RxPinOnUNO = 9;
static constexpr int RF_TxPinOnUNO = 11;
SoftwareSerial RF(RF_RxPinOnUNO, RF_TxPinOnUNO);
#endif

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

GMiniXBeeClient xbc(0x0, 0xFFFF); // Broadcast address, but this is not relevant here.
uint32_t mySH, mySL;

bool checkAnXBeeModule(uint32_t sh, uint32_t sl, bool printConfig=true) {
	bool result = false;
	bool anyChange;
	bool isLocal = (((mySH == sh) && (mySL == sl)) || ((sh == 0) && (sl == 0)));
	if (isLocal) {
		sh = sl = 0;
		Serial << "Checking local module..." << ENDL;
	} else {
		Serial << "Connecting to ";  PrintHexWithLeadingZeroes(Serial, sh);
		Serial << " - ";  PrintHexWithLeadingZeroes(Serial, sl);  Serial << ENDL;
	}

	if (xbc.isModuleOnline(sh, sl)) {
		if (printConfig) xbc.printConfigurationSummary(Serial, sh, sl);
		result = xbc.checkXBeeModuleConfiguration(sh, sl);
	} else {
		Serial << "Module not online" << ENDL;
		return true;
	}

  // At this point, we have an accessible module with NOK configuration. 
	if (!result) {
		if (xbc.getXBeeSystemComponent(sh, sl) == GMiniXBeeClient::GMiniComponent::Undefined) {
			// In this case, we cannot possibly fix the configuration.
			Serial << "*** Module is not an identified System Component *** " << ENDL;
			Serial << "*** Configuration displayed for information only.     *** " << ENDL;
			result = true;
		} else {
			if (CSPU_Test::askYN("*** Module configuration not OK. Do you want to fix it")) {
				result=xbc.checkAndFixXBeeModuleConfiguration(anyChange, sh, sl);
				if (anyChange) {
					Serial << "Saving updated configuration..." << ENDL;
					xbc.saveConfiguration(sh, sl);
					Serial << "Updated configuration is now:" << ENDL;
					xbc.printConfigurationSummary(Serial, sh, sl);
				} else Serial << "No change applied in configuration" << ENDL;
			} else {
				Serial << "Configuration unchanged." << ENDL;
			}
		}
	} else { // !result
	  Serial << "Configuration OK" << ENDL; 
	}

	return result;
}

// true is succesfull, false otherwise.
bool printConfigAndChangeBaudRateIfRequired() {
	bool failure = false;
	if (!xbc.printConfigurationSummary(Serial)) {
		Serial << F("Assuming XBee serial baud rate is 9600...") << ENDL;
		RF.end();
		RF.begin(9600);
		while (!RF);
		if (!xbc.printConfigurationSummary(Serial)) {
			Serial << F("Error printing configuration summary") << ENDL;
			failure = true;
		}
	}
	return (!failure);
}

void setup() {
	DINIT(115200);
	Serial << ENDL;
	Serial << F("Configuration utility for G-Mini XBee modules") << ENDL;
	Serial << F("---------------------------------------------") << ENDL;
  if (pin_XBeeSleepRequest != 0) {
    Serial << "SLEEP_RQ pin (#9) connected to µC pin #" << pin_XBeeSleepRequest  << ENDL;
    Serial << "Configuring SLEEP_RQ pin to LOW..." << ENDL;
    pinMode(pin_XBeeSleepRequest, OUTPUT);
    digitalWrite(pin_XBeeSleepRequest, LOW);
    delay(500);
  } else  {
      Serial << "SLEEP_RQ pin (#9) not connected"  << ENDL;
  }
	RF.begin(115200);
	xbc.begin(RF); // no return value
 Serial << F("Initialisation OK, assuming XBee serial baud rate is 115200.") << ENDL;
	Serial << "Current configuration of the XBee module:" << ENDL;
	if (!printConfigAndChangeBaudRateIfRequired()) {
		Serial.flush();
		exit(-1);
	}
	// Get my own module's address to avoid looking for it remotely
	if (!xbc.queryParameter("SH", mySH) || !xbc.queryParameter("SL", mySL)) {
		Serial << "*** Error: could not read the address of the local module (Aborted)" << ENDL;
		Serial.flush();
		exit(-1);
	}
}

void loop() {
	bool allModulesOK = true;

	Serial << ENDL <<  "1. Local XBee module" << ENDL;
	if (!checkAnXBeeModule(mySH, mySL, false)) allModulesOK = false;

	if (CSPU_Test::askYN("Check configuration of remote XBee modules (if any in range)?", true)) {

		Serial << ENDL <<  "2. Can XBee module" << ENDL;
		if (!checkAnXBeeModule(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL)) allModulesOK = false;

		Serial << ENDL <<  "3. Ground XBee module (RF-Transceiver)" << ENDL;
		if (!checkAnXBeeModule(GM_Ground_XBeeAddressSH, GM_Ground_XBeeAddressSL)) allModulesOK = false;

		Serial << ENDL <<  "4a. Subcan 1 XBee module (RF-Transceiver)" << ENDL;
		if (!checkAnXBeeModule(GM_SubCan1_XBeeAddressSH, GM_SubCan1_XBeeAddressSL)) allModulesOK = false;

		Serial << ENDL <<  "4b. Subcan 2 XBee module (RF-Transceiver)" << ENDL;
		if (!checkAnXBeeModule(GM_SubCan2_XBeeAddressSH, GM_SubCan2_XBeeAddressSL)) allModulesOK = false;

		if (CSPU_Test::askYN("Check configuration of spare XBee modules (if any in range)?", true)) {

			Serial << ENDL <<  "5a. Spare Ground XBee module (RF-Transceiver2)" << ENDL;
			if (!checkAnXBeeModule(GM_Ground2_XBeeAddressSH, GM_Ground2_XBeeAddressSL)) allModulesOK = false;

			Serial << ENDL <<  "5b. SubCan3 XBee module" << ENDL;
			if (!checkAnXBeeModule(GM_SubCan3_XBeeAddressSH, GM_SubCan3_XBeeAddressSL)) allModulesOK = false;

			Serial << ENDL <<  "5c. SubCan4 XBee module" << ENDL;
			if (!checkAnXBeeModule(GM_SubCan4_XBeeAddressSH, GM_SubCan4_XBeeAddressSL)) allModulesOK = false;
		}
	}

	if (allModulesOK) {
		Serial << ENDL << "Configuration of all accessible and identified modules OK." << ENDL;
	} // allModulesOK

	if (!CSPU_Test::askYN("Do you want to check again ?")) {
		Serial << "End of job." << ENDL;
		Serial.flush();
		exit(0);
	}
}
