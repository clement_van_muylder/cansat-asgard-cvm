/*
    Utility program to display the key-elements of the configuration of an XBee module.

     Wiring: µC to XBee module.
     Feather/ItsyBitsy
	   3.3V to VCC
	   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
	   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
	   GND  to GND
	 Uno: (RX-TX pins can be modified with constants below)
	   3.3V    to VCC
	   RX=D9   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
	   TX=D11  to DIN   CONFIRMED although it is the opposite to connect to XCTU!
	   GND     to GND

*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GMiniConfig.h"
#include "GMiniXBeeClient.h"
#include "CSPU_Test.h"

#ifdef ARDUINO_ARCH_SAMD
HardwareSerial &RF = Serial1;
#else
#   include "SoftwareSerial.h"
static constexpr int RF_RxPinOnUNO = 9;
static constexpr int RF_TxPinOnUNO = 11;
SoftwareSerial RF(RF_RxPinOnUNO, RF_TxPinOnUNO);
#endif

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

GMiniXBeeClient xbc(0, 0xFFFF); // Broadcast address, but this is not relevant here.
uint32_t mySH, mySL;

void documentRemoteModule(const uint32_t &sh, const uint32_t& sl) {
  if ((mySH != sh) || (mySL != sl)) {
    Serial << "Connecting to ";  PrintHexWithLeadingZeroes(Serial, sh);
    Serial << " - ";  PrintHexWithLeadingZeroes(Serial, sl);  Serial << ENDL;
    if (xbc.isModuleOnline(sh, sl)) {
      xbc.printConfigurationSummary(Serial, sh, sl);
      Serial<< "(";
      xbc.printSleepModeConfiguration(Serial, sh, sl);
      Serial << ")" << ENDL;
    } else Serial << "Module not online" << ENDL;
  } else Serial << "This is the local module (see above)" << ENDL;
}

void setup() {
  DINIT(115200);
  bool done = false;
  if (pin_XBeeSleepRequest != 0) {
    Serial << "SLEEP_RQ pin (#9) connected to µC pin #" << pin_XBeeSleepRequest  << ENDL;
    Serial << "Configuring SLEEP_RQ pin to LOW..." << ENDL;
    pinMode(pin_XBeeSleepRequest, OUTPUT);
    digitalWrite(pin_XBeeSleepRequest, LOW);
    delay(500);
  } else  {
    Serial << "SLEEP_RQ pin (#9) not connected"  << ENDL;
  }
  RF.begin(115200);
  xbc.begin(RF); // no return value

  Serial << F("Initialisation OK, assuming XBee serial baud rate is 115200.") << ENDL;
  if (!xbc.isModuleOnline()) {
    Serial << F("Assuming XBee serial baud rate is 9600...") << ENDL;
    RF.end();
    RF.begin(9600);
    while (!RF);
    if (!xbc.isModuleOnline()) {
      Serial << F(" *** Error: local XBee Module not found. (Aborted)") << ENDL;
      Serial.flush();
      exit(-1);
    }
  }

  while (!done) {
    Serial << ENDL <<  "1. Local XBee module" << ENDL;
    if (!xbc.printConfigurationSummary(Serial)) {
      Serial << F("Error printing configuration summary") << ENDL;
    }

    bool displayRemote = CSPU_Test::askYN("Display configuration of remote XBee modules (if any in range)?", true);

    if (displayRemote) {
      // Get my own module's address to avoid looking for it remotely
      if (!xbc.queryParameter("SH", mySH) || !xbc.queryParameter("SL", mySL)) {
        Serial << "*** Error: could not read the address of the local module (Aborted)" << ENDL;
        Serial.flush();
        exit(-1);
      }

      Serial << ENDL <<  "2. Can XBee module" << ENDL;
      documentRemoteModule(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL);

      Serial << ENDL <<  "3. Ground XBee module (RF-Transceiver)" << ENDL;
      documentRemoteModule(GM_Ground_XBeeAddressSH, GM_Ground_XBeeAddressSL);

      Serial << ENDL <<  "4a. SubCan1 XBee module" << ENDL;
      documentRemoteModule(GM_SubCan1_XBeeAddressSH, GM_SubCan1_XBeeAddressSL);

      Serial << ENDL <<  "4b. SubCan2 XBee module" << ENDL;
      documentRemoteModule(GM_SubCan2_XBeeAddressSH, GM_SubCan2_XBeeAddressSL);
    }

    bool displaySpares = CSPU_Test::askYN("Display configuration of spare XBee modules (if any in range)?", true);

    if (displaySpares) {
      Serial << ENDL <<  "5a. Spare Ground XBee module (RF-Transceiver2)" << ENDL;
      documentRemoteModule(GM_Ground2_XBeeAddressSH, GM_Ground2_XBeeAddressSL);

      Serial << ENDL <<  "5b. SubCan3 XBee module" << ENDL;
      documentRemoteModule(GM_SubCan3_XBeeAddressSH, GM_SubCan3_XBeeAddressSL);

      Serial << ENDL <<  "5c. SubCan4 XBee module" << ENDL;
      documentRemoteModule(GM_SubCan4_XBeeAddressSH, GM_SubCan4_XBeeAddressSL);
    }

    done = !CSPU_Test::askYN("Display again ? ", true);

  } // while

  Serial << ENDL << F("End of job") << ENDL;
}

void loop() {}
