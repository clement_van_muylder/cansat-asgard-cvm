/*
    This program tests the voltage on the A0 pin
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
float Vmax = 3.3;       // Ref. voltage for ADC (differs according to HW architecture)
uint16_t Nsteps;    // Number of steps of ADC (differs according to HW architecture)

float V_A;
int sensorValue_A;

void setup() {
  // to know wich Vmax and Nsteps we have to use;
  analogReference (AR_EXTERNAL);
#ifdef ARDUINO_AVR_UNO
  Nsteps = 1024;
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_ITSYBITSY_M0) || defined(ARDUIN_ITSYBITSY_M4)
  Nsteps = 4096 ;
  analogReadResolution(12);
#else
#error "Board not recognised"
#endif

  DINIT(115200);

}

void readDataA0() {
  const int Resistor2 = 10000;
  sensorValue_A = analogRead(A0);
  V_A = sensorValue_A * (Vmax / (Nsteps - 1));
}
void loop() {
  // put your main code here, to run repeatedly:
  readDataA0();
  Serial << sensorValue_A << "," << V_A << "V, " << ENDL;

  delay(1000);

}
