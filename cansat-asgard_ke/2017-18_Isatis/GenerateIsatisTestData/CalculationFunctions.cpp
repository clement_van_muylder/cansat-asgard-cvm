#include "CalculationFunctions.h"
#include "Settings.h"
#include "Arduino.h"


unsigned long addTimeJitter(unsigned long t) {
  return t+ random(-MaxTimeJitter, MaxTimeJitter);
}

float addAltitudeJitter(float alt) {
  return alt + ((float) random(-500, 500)/1000.0)*MaxAltitudeJitter;
}

unsigned long getMeasurementDuration(unsigned long t) {
  long duration=DurationOfMeasurement + random(-maxDurationJitter, maxDurationJitter); 
  bool excess= (random(0,1001) < (int) (rateOfExcessivelyLongMeasures*1000.0));
  if (excess) duration+=ExtraDurationForLongMeasures; 
  return t + duration;
}

// Return pressure in hPa.
float getPressureFromAltitude(float altitudeFromGround){
  double p=GroundLevelPressureInPa;
  p*=exp( (-7.0*GravitationalConstant*altitudeFromGround)/(2.0*1006.0*GroundLevelTemperatureInK)); 
  p = p/100 + ((double) random(-500, 500)/1000.0)*MaxPressureJitterIn_hPa;
  return p;
}

//Return Temperature in °C
float getTemperatureFromAltitude(float altitudeFromGround) {
  float temp=GroundLevelTemperature - (altitudeFromGround/155.0);
  temp+=((double) random(-500, 500)/1000.0)*MaxTemperatureJitter;
  return temp;
}


float getGPY_OutputV(float relativeAltitude)
{
    float voltage=MaxGPY_OutputV - (GPY_DeltaOutputV * relativeAltitude / MaxDeltaAltitude);
    voltage+=(random(-500, 500)/1000.0)*MaxOutputV_Jitter;
    return voltage;
}

bool getGPY_Quality() {
  return (random(0,1001) > (int) (RateOfBadQualityGPY_Readings*1000.0)); 
}

