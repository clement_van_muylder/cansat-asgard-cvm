/*
    Small test program to check verything is OK in Analog-to-Digital conversion

*/

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "HardwareScanner.h"

HardwareScanner hw;
int testID;

void runTest(int testID)
{
  static int localCurrentTest = 0;
  switch (testID) {
    case 1:
      if (testID != localCurrentTest) {
        Serial << "1. Testing with default internal reference" << ENDL;
        Serial << "Connect A0 to several tensions to check..." << ENDL;
        localCurrentTest = testID;
        analogReference(AR_DEFAULT);
        delay(1000);
      }
      {
        auto val = analogRead(A0);
        float tension = val * hw.getDefaultReferenceVoltage() / hw.getNumADC_Steps() ;
        Serial << "   A0=" << val << "/" << hw.getNumADC_Steps()  << ", reading " << tension << "V" << ENDL;
      }
      break;
    case 2:
      if (testID != localCurrentTest) {
        Serial << "2. Testing with external reference" << ENDL;
        Serial << "Connect AREF to reference tension (!! <=3.3V)" << ENDL;
        Serial << "Connect A0 to several tensions to check..." << ENDL;
        localCurrentTest = testID;
        analogReference (AR_EXTERNAL);
        delay(1000);
      }
      {
        auto val = analogRead(A0);
        float ratio = ((float) val) / hw.getNumADC_Steps() ;
        Serial << "   A0=" << val << "/" << hw.getNumADC_Steps()  << " = " << ratio << " VRef" << ENDL;
      }
      break;
    case 3:
      Serial << "Test over." << ENDL;
      exit(0);
      break;
    default:
      Serial << "Unsupported test ID:" << testID << " (Aborted)" <<  ENDL;
      exit(-1);
  } // switch
}
void setup() {
  DINIT(115200);
  Serial << "Initializing Hardware scanner. Remember this is VERY slow if pullups are not installed on SCL and SDA! ..." << ENDL;
  hw.init();
  hw.printFullDiagnostic(Serial);
  Serial << ENDL;
  Serial << "==== Analog-Digital conversion test on A0, assuming " << hw.getNumADC_Steps() << " steps, VREF=" << hw.getDefaultReferenceVoltage() << "V) ====" << ENDL;
  testID = 1;
  // remove any previous character in input queue
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial << "Enter any character to switch to next test..." << ENDL ;
  delay(500);
}

void loop() {
  runTest(testID);
  if (Serial.available()) {
    while (Serial.available() > 0) {
      Serial.read();
    }
    testID++;
  }
  delay(500);
}
