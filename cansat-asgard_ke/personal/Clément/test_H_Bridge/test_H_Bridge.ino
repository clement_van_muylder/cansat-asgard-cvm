// Constants
const int enableBridge1 = 9;
const int MotorForward1 = 6;
const int MotorReverse1 = 5;
const int DelayForward = 6000;
const int DelayReverse = 5000;
const int DelayStop = 4000;
// Variables
int Power = 255; //Motor velocity between 0 and 255
void setup(){
pinMode(MotorForward1,OUTPUT);
pinMode(MotorReverse1,OUTPUT);
pinMode(enableBridge1,OUTPUT);
}
void loop(){
digitalWrite(enableBridge1,HIGH); // Active pont en H
// Tourne dans le sens direct pendant 2 secondes
analogWrite(MotorReverse1,0);
analogWrite(MotorForward1,Power);
delay(DelayForward);
// Tourne dans le sens indirect pendant 3 secondes
analogWrite(MotorForward1,0);
analogWrite(MotorReverse1,Power);
delay(DelayReverse);
//Arrête le moteur pendant 1 seconde
analogWrite(MotorForward1,0);
analogWrite(MotorReverse1,0);
digitalWrite(enableBridge1,LOW);
delay(DelayStop);
}
