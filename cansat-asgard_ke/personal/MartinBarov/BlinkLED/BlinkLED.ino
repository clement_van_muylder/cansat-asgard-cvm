#include"BlinkLED.h"
BlinkLED led1;
BlinkLED led2;
BlinkLED led3;

void setup() {
  led3.begin(9, 2000);
  led1.begin(10, 2000);
  led2.begin(11, 2000);
}
void loop() {
  led1.run();
  led2.run();
  led3.run();
}
