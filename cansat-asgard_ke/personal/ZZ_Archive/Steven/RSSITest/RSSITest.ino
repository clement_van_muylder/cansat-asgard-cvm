// rf22_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing client
// with the RF22 class. RF22 class does not provide for addressing or reliability.
// It is designed to work with the other example rf22_server

#include <SPI.h>
#include <RH_RF22.h>
#include "Arduino.h"

// Singleton instance of the radio
RH_RF22 rf22(5, 10);

double readings = 0.0;
int currentCh = 0;
unsigned int i = 0;
unsigned long timestamp = 0;

void setup() 
{
  Serial.begin(9600);
  while(!Serial);
  Serial.println("START");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  if (!rf22.init()){
    Serial.println("RF22 init failed");
    while(!rf22.init()){
      timestamp = millis();
      Serial.print(".");
      delay(500);
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }
  else{
    Serial.println("Init successed");
  }
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  rf22.setModeRx();
  Serial.print("Mode changed for : ");
  Serial.println(rf22.mode());
  rf22.setFrequency(240+72.00*currentCh);
  if(!rf22.setFrequency(433)){
    Serial.println("*** Frequency not changed! ***");
    delay(1000);
    while(!rf22.setFrequency(433)){
      timestamp = millis();
      Serial.print(".");
      delay(500);
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }
}

void loop()
{
  Serial.println("Receiving...");
  for (unsigned int ts = millis(); (millis() - ts) < 3000; i++){
    Serial.print("Channel ");
    Serial.print(currentCh);
    
    /*if(!rf22.isChannelActive()){
      Serial.print(" not active");
    }*/
    
    Serial.println();
    digitalWrite(LED_BUILTIN, HIGH);
    readings = rf22.rssiRead();
    Serial.print("rssi ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.println(readings);
    Serial.print("dbm : ");
    Serial.println(0.5*readings-131);
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(500);
  }
  i=0;
  //rf22.clearRxBuf();
  
  currentCh++;
  if(currentCh == 11){
    currentCh = 0;
  }
  rf22.setFrequency((433+72.00*currentCh));
  if(!rf22.setFrequency(0)){
    timestamp = millis();
    Serial.println("*** Frequency not changed! ***");
    while(!rf22.setFrequency(240+72.00*currentCh)){
      Serial.print(".");
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }
  /*else {
    Serial.println("Nothing");
  }*/
}
