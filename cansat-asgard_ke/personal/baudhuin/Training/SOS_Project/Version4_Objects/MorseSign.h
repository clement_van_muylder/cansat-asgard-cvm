/*
 *  A class implementing a Morse sign repeat n times by a led. 
 *  
 *  Usage:
 *  
 *    MorseSign sign;
 *    sign.start(LED_BUILTIN, Dash, 4);
 *     
 *    // Call run in loop until it returns true.
 *    signComplete= sign.run();
 */

class MorseSign {
  public:
    enum MorseSign_t {
      Dot,
      Dash
    };
    MorseSign() {};

    void start(byte theLedNumber, MorseSign_t theSign, byte theNumOfRepetitions ) {
      ledNumber = theLedNumber;
      numRepetitions = theNumOfRepetitions;
      signType = theSign;
      counter = 0;
      state = Off;
      timestamp = 0L;
      digitalWrite(ledNumber, LOW);
      Serial.print("Starting sign, Dash=");
      Serial.println(signType == Dash);
    }

    bool run() {
      switch (state)
      {
        case On:
          if ( ((millis() - timestamp) >= DashDuration)
               || ((signType == Dot) && ((millis() - timestamp) >= DotDuration)) ) {
            digitalWrite(ledNumber, LOW);
            timestamp = millis();
            state = Off;
            counter++;
          }
          break;
        case Off:
          //Serial.print("Counter = ");
          //Serial.println(counter);
          if ((millis() - timestamp) >= InterSignDelay) {
            if (counter == numRepetitions) {
              state = Done;
            } else {
              Serial.println("switching ON");
              digitalWrite(ledNumber, HIGH);
              timestamp = millis();
              state = On;
            }
          }
          break;
        case Done:
          break;
        default:
          Serial.println("Unexpected sign state");
      }
      return (state == Done);
    }
  private:
    static constexpr unsigned int DotDuration = 150; // msec
    static constexpr unsigned int DashDuration = 500; // msec
    static constexpr unsigned int InterSignDelay = 150; // msec.
    enum MorseSignState_t {
      On,
      Off,
      Done
    };

    byte ledNumber;
    byte numRepetitions;
    MorseSign_t signType;
    MorseSignState_t state;
    byte counter;
    unsigned long timestamp;
};
