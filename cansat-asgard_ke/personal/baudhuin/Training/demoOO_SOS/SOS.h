#pragma once
#include "Arduino.h"

/** A class managing a LED to emit SOS signals */
class SOS {
  public:
    static constexpr unsigned long delayBetweenSOS = 2000; // msec
    static constexpr unsigned long dotDuration = 200; // msec
    static constexpr unsigned long dashDuration = 600; // msec
    static constexpr unsigned long delayBetweenSigns = 200; // msec

    SOS(byte pinNumber);
    void run();
  protected:
    // C++11 enum Class is better than plain C-style enums !
    enum class SOS_State_t {
      firstS_Character,
      O_Character,
      secondS_Character,
      waiting
    };

    /** Initiate a group of 3 signs. Call threeSigns() until it returns true 
        to complete the job.  */
    void startThreeSigns();
    
    /** Run a cycle of three signs of duration msec, each followed by a
        pause of delayBetweenSigns msec. Call startThreeSigns() to (re-)start
        a cycle.
        @return true if the cycle is over, false if additional calls to threeSigns()
                     are required. 
    */
    bool threeSigns(unsigned long duration);

  private:
    byte pinNumber;
    SOS_State_t state;
    byte counter;
    unsigned long ts;
} ;
