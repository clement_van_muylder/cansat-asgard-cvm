#define DeBUG_CSPU
#include "DebugCSPU.h"
#include "XBeeClient.h"
#include "CSPU_Test.h"

XBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
HardwareSerial &RF = Serial1;

void setup() {
  DINIT(115200);
  RF.begin(115200);
  xbc.begin(RF); // no return value
  xbc.configureSleepMngtPins(pin_XBeeSleepRequest, pin_XBeeOnSleep, true);
}

void loop() {
  CSPU_Test::pressAnyKey("Ready to assert sleepRequest pin");
  digitalWrite(pin_XBeeSleepRequest, HIGH);
  CSPU_Test::pressAnyKey("Ready to de-assert sleepRequest pin");
  digitalWrite(pin_XBeeSleepRequest, LOW);
}
