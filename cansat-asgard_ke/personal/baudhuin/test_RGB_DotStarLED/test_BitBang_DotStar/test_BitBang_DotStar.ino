/*  Test program for bitbanging the RGB LED (to shut it down). 
 *   We must transfert  00 00 00 00 FF 00 00 00 FF  to shutdown the 1 pixel. 
 *   Data is valid on the rising edge of the clock. 
*/
#ifndef ARDUINO_ITSYBITSY_M4
#error "Test currently only supports ItsyBitsy M4"
#endif

#define CSPU_DEBUG
#include "DebugCSPU.h"
#include "CSPU_Test.h"

constexpr uint8_t DataPin=8;
constexpr uint8_t ClockPin=6;

void sendBit(bool high, uint16_t numRepeats)
{
   digitalWrite(ClockPin, LOW);
   digitalWrite(DataPin, high ? HIGH : LOW);
   delay(5);
   for (int i = 0; i < numRepeats; i++) {
       digitalWrite(ClockPin, HIGH); 
       delay(5);
       digitalWrite(ClockPin, LOW);
       delay(5);
       Serial<< (high ? 1:0) ;
   }
   Serial << ENDL;
}

void shutdownDotStar() {
  sendBit(0, 32); // start frame
  sendBit(1, 8); // start LED
  sendBit(0, 24); // RBG
  sendBit(1, 8); // End frame.
}

void setup() {
  DINIT(115200);
  pinMode(DataPin, OUTPUT);
  pinMode(ClockPin, OUTPUT);

  Serial << "ready to bit bang..." ;
  CSPU_Test::pressAnyKey();

  Serial << "Turning off RGB LED..." << ENDL;
  shutdownDotStar();
  
  Serial << "Dotstar now shut down" << ENDL;
  CSPU_Test::pressAnyKey();
  Serial << "End of job " << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
