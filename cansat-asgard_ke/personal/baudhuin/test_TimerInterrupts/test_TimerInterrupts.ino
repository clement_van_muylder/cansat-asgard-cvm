/* 
 *  Test timer interrupts on Feather and Arduino
 *  Blink a LED on 
 */

#define DEBUG_CSPU
#include "DebugCSPU.h"

constexpr byte ledPin=LED_BUILTIN; 

int timer1_counter;                            // Used for simulated sensor pulses
int pulseCounter=0;

void setup() {
  DINIT(115200);
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  timer1_counter = 63661;   // preload timer 65536-16MHz/256/ = 25Hz. Change this value for the required interrupt rate
  TCNT1 = timer1_counter;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts 
}

ISR(TIMER1_OVF_vect)        // interrupt service routine used only for simulated sensor pulses
{
  TCNT1 = timer1_counter;                         // preload timer
  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);  // Flash LED at 25Hz
  pulseCounter++;
}


void loop() {
  // put your main code here, to run repeatedly:
  Serial << millis() << ": "<<  pulseCounter << " freq=" << ((float) pulseCounter)/millis()*1000.0 << " Hz" << ENDL;
  delay(500);
}
