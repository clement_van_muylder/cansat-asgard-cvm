#include "Arduino.h"
class BlinkLED {
  public:
    bool begin( const uint8_t TheLedPinNbr, uint16_t APeriodInMsec);
    void run();
    void flash();

  private:
    

    uint16_t ledPinNbr;
    unsigned long lastChangeTimestamp ;
    uint8_t periodInMsec;

    
};
