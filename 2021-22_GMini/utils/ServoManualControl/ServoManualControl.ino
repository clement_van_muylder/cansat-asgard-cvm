#include "CSPU_Test.h"
#include "DebugCSPU.h"
#include "GMiniConfig.h"
#include "ServoLatch.h"

ServoLatch myServo;
<<<<<<< HEAD
int neutralPosition = 83;
int amplitude = 30;
const int enableBridge1 = ; 
=======
>>>>>>> 58ab55183b1d5f12498bcc8b0b3447234dc5fc68

auto neutralPosition=ServoLatchNeutralPosition; 
auto amplitude=ServoLatchAmplitude;

void setup() {
  DINIT(115200);
<<<<<<< HEAD
  myServo.begin(6);
=======
 
<<<<<<< HEAD
  myServo.begin(5, ServoActivationPin);
>>>>>>> d6e1e5dfa83d998835ba89e2b07ddb25bf398152
  myServo.configure(neutralPosition, amplitude);
=======
  myServo.begin(ServoLatchPWM_Pin, ServoLatchActivationPin);
  myServo.configure(ServoLatchNeutralPosition, ServoLatchAmplitude);
>>>>>>> 58ab55183b1d5f12498bcc8b0b3447234dc5fc68
  Serial << "Manual latch control program" << ENDL;
  Serial << "----------------------------" << ENDL;
  Serial << "Write C to increase the neutral position by one degree." << ENDL;
  Serial << "Write c to decrease the neutral position by one degree." << ENDL;
  Serial << "Write A to incraese the amplitude by one degree." << ENDL;
  Serial << "Write a to decrease the amplitude by one degree." << ENDL;
  Serial << "Write u to call the unlock function." << ENDL;
  Serial << "Write l to call the lock function." << ENDL;
  Serial << "Write n to call the neutral function." << ENDL;
}

void loop() {
  char c = CSPU_Test::keyPressed() ;
  switch (c) {
    case 'C':
      neutralPosition++;
      Serial << "neutral position now at " << neutralPosition << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.neutral();
      break;
    case 'c':
      neutralPosition--;
      Serial << "neutral position now at " << neutralPosition << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.neutral();
      break;
    case 'A':
      amplitude++;
      Serial << "amplitude now at " << amplitude << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.lock();
      break;
    case 'a':
      amplitude--;
      Serial << "amplitude now at " << amplitude << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.lock();
      break;
    case 'u':
      Serial.println("Unlock");
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      myServo.unlock();
      break;
    case 'l':
      Serial.println("Lock");
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      myServo.lock();
      break;
    case 'n':
      Serial.println("Neutral");
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      myServo.neutral();
      break;
    case 'h':
      Serial.println("Toggling H-bridge");
      digitalWrite(ServoLatchActivationPin, !digitalRead(ServoLatchActivationPin));
      break;
    default:
      ;// do nothing
  }

}
