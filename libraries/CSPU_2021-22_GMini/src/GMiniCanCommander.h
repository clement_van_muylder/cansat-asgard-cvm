/*
   GMiniCanCommander.h
*/

#pragma once
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
    /* v1.1.8 of the latch library does not support the SAMD51 processor. See note in class comment */
#  define GMINI_SUPPORT_LATCH_COMMANDS
#else
#  undef GMINI_SUPPORT_LATCH_COMMANDS
#endif
#include "RT_CanCommander.h"

#ifdef GMINI_SUPPORT_LATCH_COMMANDS
#include "Latch.h"
#endif 

/** @ingroup GMiniCSPU
    @brief A subclass of RT_CanCommander adding support for the GMini-specific commands
    as specified in CansatInterface.h
*/
class GMiniCanCommander : public RT_CanCommander {
  public:
    /** @brief Constructor
          @param theTimeOut The duration (in milliseconds) for the Command mode to time out (and switch back to acquisition).
    */
    GMiniCanCommander(unsigned long int theTimeOut);
    virtual ~GMiniCanCommander() {};

#ifdef RF_ACTIVATE_API_MODE
    /** @brief Method used to initialize the object when using the RF API mode.
        @param xbeeClient The interface to the XBee module to used for output .
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.
        @param theServo   The asynchronous servoWinch object that can be targeted with commands.
    */
    void begin(CansatXBeeClient& xbeeClient, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL);
#else
    /** @brief Method used to initialize various pointers.
        @param RF_Stream The output stream used by the class.
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.
        @param theServo   The asynchronous servoWinch object that can be targeted with commands.
    */
    void begin(Stream& RF_Stream, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL);
#endif


  protected:
    /** Process a project specific command request while in command mode.
        This method implements GMini-specific commands.
       @param requestType the request type value
       @param cmd Pointer to the first character after the command type. Command parameter can be parsed from
              this position which should point to a separator if any parameter is present, or to the final '\0'
              if none is provided.
       @return True if the command was processed, false otherwise.
    */
    virtual bool processProjectCommand(CansatCmdRequestType requestType, char* cmd)  override;
#ifdef GMINI_SUPPORT_LATCH_COMMANDS
    /** process the lock command */
    void processReq_Latch_Lock(char* &nextCharAddress);

    /** process the unlock command */
    void processReq_Latch_Unlock(char* &nextCharAddress);

    /** obtain the status of the servo */
    void processReq_GetLatchStatus(char* &nextCharAddress);

    /** set the latch of the servo to neutral position */
    void processReq_Latch_SetNeutral(char* &nextCharAddress);
#endif
  private:
#ifdef GMINI_SUPPORT_LATCH_COMMANDS
    Latch* latch; /**< the servolatch object managed in the can */
#endif

};
