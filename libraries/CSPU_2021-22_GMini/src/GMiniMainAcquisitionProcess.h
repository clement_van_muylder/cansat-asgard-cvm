/*
   GMiniAcquisitionProcess.h
*/

#pragma once
#include "GMiniAcquisitionProcess.h"
#include "GMiniSecondaryMissionController.h"
#include "Latch.h"

/** @brief the Aquisition process of the main can of Gmini project
*/
class GMiniMainAcquisitionProcess: public GMiniAcquisitionProcess {
  public:
    GMiniMainAcquisitionProcess() {};
    virtual ~GMiniMainAcquisitionProcess() {};


    /** Obtain the ServoLatch object managed by the secondary mission controller */
   Latch* getLatch() {
      return theGMiniController.getLatch();
    };
  private:
    /** @name Private methods overridden from base class.
      	See detailed description in base classes.
        @{   */

    virtual void acquireSecondaryMissionData(CansatRecord &record) override {
      ((GMiniRecord&) record).sourceID = CansatCanID::MainCan;
    } ;

    virtual SecondaryMissionController* getSecondaryMissionController() override {
      return &theGMiniController;

    };
    /** @} */
    GMiniSecondaryMissionController theGMiniController; // Our secondaryMissionController.

};
