/*
   GMiniSecondaryMissionController.cpp
*/

// This class is not supported on SAMD51 (e.g. ItsyBisty M4) because servos are not
// supported (as of 202203) on SAMD51 boards.
#ifndef __SAMD51__

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GMiniSecondaryMissionController.h"
#define DBG_VELOCITY_BUFFER 0
#define DBG_VELOCITY 0
#define DBG_EJECTION 0
void GMiniSecondaryMissionController::printVelocityTable() {
  for (int i = 0; i < NumberOfVelocitySamples; i++)
    Serial << "  " << i << ": " << velocityTable[i] << ENDL;
}

void GMiniSecondaryMissionController::updateStatus(GMiniRecord& rec) {
#if (DBG_VELOCITY_BUFFER == 1)
  printVelocityTable();
#endif
#if (DBG_VELOCITY == 1)
  Serial << "BEFORE: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ", lastTs=" << lastRecordTimestamp << ENDL;
#endif
  if (rec.descentVelocity == BMP_Client::InvalidDescentVelocity) {
#if (DBG_VELOCITY == 1)
    Serial << "INVALID VELOCITY DETECTED velocity= " <<  rec.descentVelocity << ENDL;
#endif
    clearAverageVelocity();
    lastRecordTimestamp = rec.timestamp;
    return;
  }
  if ((rec.timestamp - lastRecordTimestamp) > (3 * CansatAcquisitionPeriod)) {
    clearAverageVelocity();
#if (DBG_VELOCITY == 1)
    Serial << "INVALID REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
    lastRecordTimestamp = rec.timestamp;
    return;
  }

  if (rec.timestamp < lastRecordTimestamp) {
    clearAverageVelocity();

#if (DBG_VELOCITY == 1)
    Serial << "INVALID PAST REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
  }
  averageVelocity = averageVelocity + (rec.descentVelocity - velocityTable[currentVelocityIndex]) / NumberOfVelocitySamples;
  velocityTable[currentVelocityIndex] = rec.descentVelocity;
  currentVelocityIndex++;
  if (currentVelocityIndex == NumberOfVelocitySamples) {
    currentVelocityIndex = 0;
    DPRINTSLN(DBG_VELOCITY, "Index reset"); //
    velocityAverageValid = true;
  }
  lastRecordTimestamp = rec.timestamp;
#if (DBG_VELOCITY_BUFFER == 1)
  Serial << "AFTER: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
  printVelocityTable();
#endif
  if ((!takeoffHappened) && velocityAverageValid && (averageVelocity  < VelocityThresholdForTakeoff )) {
    DPRINTLN(DBG_EJECTION, takeoffHappened);
    DPRINTLN(DBG_EJECTION, velocityAverageValid);
    DPRINTLN(DBG_EJECTION, averageVelocity);
    DPRINTLN(DBG_EJECTION, VelocityThresholdForTakeoff);
    takeoffHappened = true;
    takeoffTimestamp = rec.timestamp;
     CansatXBeeClient *xbee=getXbeeClient();
    if(xbee != nullptr){
      RF_OPEN_STRING(xbee);
      *xbee << millis() << ": 2-ary: Take off detected !";
      RF_CLOSE_STRING(xbee);
    }
    DPRINTS(DBG_EJECTION, "takeoff : timestamp set to ");
    DPRINT(DBG_EJECTION, takeoffTimestamp);
    DPRINTS(DBG_EJECTION, ", threshold (m/s) ");
    DPRINTLN(DBG_EJECTION, VelocityThresholdForTakeoff);

  }

}


void GMiniSecondaryMissionController::manageSecondaryMission(CansatRecord & record)
{
  static uint16_t counter=0;
  if (counter < 10000/CansatAcquisitionPeriod) {
    counter++;
    return;
  }
  GMiniRecord& myGMiniRecord = (GMiniRecord&) record;
  updateStatus(myGMiniRecord);

  if ((!subCanEjected) && (mustEject(myGMiniRecord))) {
    DPRINTSLN(DBG_EJECTION, "Ejection");
   CansatXBeeClient* xbee=getXbeeClient();
    if(xbee != nullptr){
      RF_OPEN_STRING(xbee);
      *xbee << millis() << ": 2-ary Ejection triggered!";
      RF_CLOSE_STRING(xbee);
    } else Serial << "ERROR: No radio" << ENDL;

    latch->unlock();
    subCanEjected = true;
  }
  myGMiniRecord.subCanEjected = subCanEjected;
}

void GMiniSecondaryMissionController::clearAverageVelocity() {
  for (int i = 0; i < NumberOfVelocitySamples; i++) {
    velocityTable[i] = 0;
  }
  averageVelocity = 0;
  velocityAverageValid = false;
}

bool GMiniSecondaryMissionController::begin(CansatXBeeClient* xbeeClient) {
  bool result = SecondaryMissionController::begin(xbeeClient);
  if (!result) return false;
  takeoffHappened = false;
  takeoffTimestamp = 0;
  subCanEjected = false;
  ServoLatch * servo = new ServoLatch;
  if (servo == nullptr) {
    DPRINTS(DBG_DIAGNOSTIC, "ERROR ALLOCATING SERVOLATCH");
    return false;
  }
  result = servo->begin(ServoLatchPWM_Pin, ServoLatchActivationPin, ServoLatchMinPulseWidth, ServoLatchMaxPulseWidth);
  if (result == false) {
    delete servo;
    return false;
  }
  servo->configure(ServoLatchNeutralPosition,ServoLatchAmplitude);
  clearAverageVelocity();
  latch = servo;
  return true;
}


bool GMiniSecondaryMissionController::mustEject(GMiniRecord& record) {
  if (!takeoffHappened) {
    return false;
  }
  if ((record.timestamp - takeoffTimestamp) > MaxDelayFromTakeoffToEjection) {
    return true;
  }
  else {
    return false;
  }

}


float GMiniSecondaryMissionController::getAverageVelocity() {
  if (velocityAverageValid == true) {
    return averageVelocity;
  }
  else {
    return InvalidVelocity;
  }
}

bool GMiniSecondaryMissionController::takeoffDetected() {
  return takeoffHappened;
}

#endif
