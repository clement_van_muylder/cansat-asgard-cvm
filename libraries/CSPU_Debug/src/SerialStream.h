/*
 * SerialStream.h
 * 
 * A couple of template function allowing to use streaming operators with Arduino's Serial objects 
 */
/** 
 *  @ingroup CSPU_Debug
 *  @{
 */
#pragma once
#include "Arduino.h"

/**@brief  Streaming operator for Arduino's Print objects (e.g. Serial).
 * 
 * This template for operator<< allows for constructs like
 * @code
 * Serial << F("GPS unit #") << gpsno << F(" reports lat/long of ") << lat << "/" << long << ENDL; 
 * @endcode
 * Source: https://playground.arduino.cc/Main/StreamingOutput 
 * @todo No support for HEX and DEC in streaming yet.
 * @remarks
 * 1/2/2018: No easy support found for HEX and DEC: defining a manipulator only works if the
 *           stream reacts on its return type to change its internal state. Since we cannot
 *           change the Print classe behaviour, only operators can be used.
 *           operator <<= allows streaming integers in hex format, but precedence rules do not
 *           allow to combine  with << as in Serial <<= anInt << " and another in " << anOtherInt;
 *           No obvious support for selection number of decimals in float either.
 */
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }

/** Utility function to print unsigned integers in HEX format with leading zeroes
 *  @param obj The stream to print to
 *  @param arg The number to print
 *  @param numBytes The number of bytes of the number (1 to 4)
 *  @param withPrefix If true, the '0x' prefix is printed before the number */
inline void PrintHexWithLeadingZeroes(Print &obj, uint32_t arg, uint8_t numBytes, bool withPrefix=true) {
	if (withPrefix) obj.print("0x");
	uint32_t mask = 0xFF << 8*(numBytes-1);
	for (int i = numBytes-1; i>=0;i--) {
		byte value= (byte) ((arg & mask) >> (8*i));
		mask >>=8;
		if (value < 0x10) obj.print(0);
		obj.print(value, HEX);
	}
}

/** Utility function to print uint32_t in HEX format with leading zeroes
 *  @param obj The stream to print to
 *  @param arg The number to print
 *  @param withPrefix If true, the '0x' prefix is printed before the number */
inline void PrintHexWithLeadingZeroes(Print &obj, uint32_t arg, bool withPrefix=true) {
	PrintHexWithLeadingZeroes(obj, arg, 4, withPrefix);}

/** Utility function to print uint16_t in HEX format with leading zeroes
 *  @param obj The stream to print to
 *  @param arg The number to print
 *  @param withPrefix If true, the '0x' prefix is printed before the number */
inline void PrintHexWithLeadingZeroes(Print &obj, uint16_t arg, bool withPrefix=true) {
	PrintHexWithLeadingZeroes(obj, arg, 2, withPrefix);}

/** Utility function to print uint8_t in HEX format with leading zeroes
 *  @param obj The stream to print to
 *  @param arg The number to print
 *  @param withPrefix If true, the '0x' prefix is printed before the number */
inline void PrintHexWithLeadingZeroes(Print &obj, uint8_t arg, bool withPrefix=true) {
	PrintHexWithLeadingZeroes(obj, arg, 1, withPrefix);}

/** Utility function to print uint64_t in HEX format with leading zeroes
 *  @param obj The stream to print to
 *  @param arg The number to print
 *  @param withPrefix If true, the '0x' prefix is printed before the number */
inline void PrintHexWithLeadingZeroes(Print &obj, uint64_t arg, bool withPrefix=true) {
	PrintHexWithLeadingZeroes(obj, (uint32_t) (arg >> 32), withPrefix);
	PrintHexWithLeadingZeroes(obj, (uint32_t) (arg & 0xFFFFFFFF), false);
}

/** Specialized version of the previous template operator<< to support uint64_t numbers.
 *  Only hexadecimal with leading zeroes is supported.
 */
inline Print &operator << (Print &obj, uint64_t arg) {
		PrintHexWithLeadingZeroes(obj, arg);
		return obj;
}

/** Specialized version of the previous template operator<< to support boolean values.
 */
inline Print &operator << (Print &obj, bool arg) {
		obj.print((arg ? "True" : "False"));
		return obj;
}


/* @cond HIDE_FROM_DOXYGEN 
 * Convenience macro to allows constructs like
 * @code Serial vv("GPS unit #") << gpsno << ENDL; @endcode
 * in order to hide the F macro. Not used in Cansat 2018-19, not very readable: do not use!
 */
#define vv(str)  << F(str)
/* Convenience macro to allows constructs like
 * @code Serial VV("GPS unit #") << gpsno << ENDL; @endcode
 * in order to hide the F macro. Not used in Cansat 2018-19, not very readable: do not use!
 */
#define VV(str)  << F(str)
/** @endcond */

/** End-of-line macro, since std::endl cannot be used. Allows constructs like
 * @code Serial << "message followed by end-of-line" << ENDL; @endcode
 */
#define ENDL "\n"

/** @} */

#ifdef NEVER
// This was an attempt at using another operator for hexadecimal display. It does not work
// because of the precedence rules and modifies the numbers...
template<class T> inline Print &operator <<=(Print &obj, T arg) { obj.print(arg, HEX); return obj; }

// This was an attempt at stream manipulator that cannot possibly be used since we cannot
// alter the Print class.
struct SetFormat_t { int format; };
/*
 *  @brief  Manipulator for Print streams.
 *  @param  c  The new fill character.
 *
 *  Sent to a stream object, this manipulator calls print(arg, format) for that
 *  object.
 */
template<class T>
inline SetFormat_t  format(T arg, int theFormat)
     {
       SetFormat_t __x;
       __x.format=theFormat;
      return __x;
 }
#endif

