/*
    Test read/write access to external EEPROM using class ExtEEPROM.

    Connexions: 2 24LC256 chips (but could be run with only one chip).
    A0 to VCC on chip A, A0 to VSS on chip B
    A1-A2 to VSS.
    As a consequence chip A is at address 1010000=0x50
                     chip B is at address 1010001=0x51
    2k pull-up resistor on SDA and SCL to operate at 400kHz or 1MHz. (10k for 100 kHz).

    Be sure to adjust the EEPROM_LastAddress value to the 0xFFFF (64k EEPROMS) or Ox7FFF (32k EEPROMs)
*/

#include "ExtEEPROM.h";
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include <Wire.h>

const byte chip[2] = { 0x50, 0x51 };  //Address of 24LC256 eeprom chips. Use the same if only one available.

const EEPROM_Address EEPROM_LastAddress= 0xFFFF;
byte numErrors = 0;

void testByteReadWrite()
{
  EEPROM_Address address = (EEPROM_Address) random(155, 16 * 1024);
  byte value = random(0, 0xfe);
  byte readValue = 0xff;
  bool result;

  Serial << F("Testing byte read/write: ") << ENDL;
  for (int i = 0 ; i < 2; i++)
  {
    Serial << F("  Chip at I2C address ") << (int) chip[i]
           << F(", address ") << address
           << F(": writing value ") << value << F(" and checking...") ;
    result = ExtEEPROM::writeByte(chip[i], address, value);
    if (!result) {
      Serial << F("*** Error writing!") << ENDL;
      numErrors++;
    }
    result = ExtEEPROM::readByte(chip[i], address, readValue);
    if (!result) {
      Serial << F("*** Error reading!") << ENDL;
      numErrors++;
    }
    if (readValue != value) {
      Serial << F("*** Error: read ") << readValue << F(", expected ") << value << ENDL;
      numErrors++;
    }
    Serial << "OK." << ENDL;
  }
  Serial << F("End of byte read/write test") << ENDL;
}

void writeAndCheck(byte I2C_Address, byte address, byte dataSize)
{
  byte startValue = random(1, 255);
  byte dataIn[20], dataOut[20];

  // 1. Compose data to be written.
  for (int i = 0; i < dataSize; i++) {
    dataIn[i] = i + startValue;
  }

  Serial << F("  Writing ") << dataSize;
  Serial << F(" bytes into EEPROM at I2C address ") << I2C_Address << F(" starting at address 0x");
  Serial.print(address, HEX);
  Serial << F(". Values start from ") << startValue << F(". ");

  byte actuallyWritten = ExtEEPROM::writeData(I2C_Address, address, dataIn, dataSize);
  if (actuallyWritten != dataSize) {
    Serial << F("*** Error: only ") << actuallyWritten << F(" bytes written!") << ENDL;
    numErrors++;
  }
  Serial << F("  Checking...");

  byte actuallyRead = ExtEEPROM::readData(I2C_Address, address, dataOut, dataSize);
  if (actuallyRead != dataSize) {
    Serial << F("*** Error: only ") << actuallyRead << F(" bytes read!") << ENDL;
    numErrors++;
  }
  for (int i = 0; i < dataSize; i++) {
    if (dataIn[i] != dataOut[i]) {
      Serial << ENDL << F("*** Error: byte ") << i << F(" should be ") << dataIn[i] << F(". Found ") << dataOut[i] << ENDL;
      numErrors++;
    }
  }
  Serial << F(" OK.") << ENDL;
}

void testPageWrite_SmallData() {
  // Write data in chunks of 0 to 18 bytes, within and across pages.
  Serial << F("Writing and checking small chunks of data...") << ENDL;
  for (byte dataSize = 0; dataSize <= 18; dataSize ++) {
    // Test at start of page (1 write).
    writeAndCheck(chip[1], (dataSize + 1) * 64, dataSize);
    // Test 1 byte before end of page.
    writeAndCheck(chip[1], (dataSize + 1) * 64 - 1, dataSize);
  }
}
void testWriteLastBytesOfChip(){
  // Write data in chunks of 0 to 18 bytes, within and across pages.
  Serial << F("Writing and checking small chunks of data at end of chips...") << ENDL;
  for (byte dataSize = 0; dataSize <= 18; dataSize ++) {
    writeAndCheck(chip[1], EEPROM_LastAddress-dataSize+1, dataSize);
  }
}

void testPageWrite_LargeData() {
  // Write a sequence of integers in chunks of dataSize bytes
  byte startValue = random(1, 255);
  byte value = startValue;
  byte dataSize = 47;
  byte data[dataSize];
  unsigned int written;
  const EEPROM_Address startAddress = 0x01ff;
  const unsigned int numToWrite = 300;
  Serial << F("Testing large write") << ENDL;
  Serial << F("  Writing ") << numToWrite << F(" bytes in blocks of ") << dataSize << F(" bytes max.") << ENDL;
  Serial << F("  into EEPROM at I2C address ") << (int) chip[0] << F(" starting at address 0x");
  Serial.print(startAddress, HEX);
  Serial << F(". Values start from ") << value << F(".");

  for (written = 0; written < numToWrite; written += dataSize) {
    for (int i = 0; i < dataSize; i++) {
      data[i] = value++;
    }
    byte chunkSize = ((numToWrite - written) < dataSize) ? numToWrite - written : dataSize;
    Serial << F("  ") << chunkSize << F(" bytes sent...") << ENDL;
    byte actuallyWritten = ExtEEPROM::writeData(chip[0], startAddress + written, (byte *) data, chunkSize);
    if (actuallyWritten != chunkSize) {
      Serial << F("*** Error: only ") << actuallyWritten << F(" bytes written!") << ENDL;
      numErrors++;
    }
  }

  // Checking: reading numToWrite bytes, in chuncks of chunkSize /2 -1 ;
  dataSize = (dataSize / 2) - 1;
  value = startValue;
  Serial << F("  Checking content of EEPROM in chuncks of ") << dataSize
         << F(" bytes expecting numbers from ") << startValue << F("....") << ENDL;
  unsigned int totalRead = 0;
  while (totalRead < numToWrite) {
    byte chunkSize = (numToWrite - totalRead) > dataSize ? dataSize : numToWrite - totalRead;
    Serial << F("  Reading ") << chunkSize << F(" bytes from address 0x");
    Serial.print(startAddress + totalRead, HEX);
    Serial << F("...") << ENDL;
    byte actuallyRead = ExtEEPROM::readData(chip[0], startAddress + totalRead, (byte *) data, chunkSize);
    if (actuallyRead != chunkSize) {
      Serial << F("*** Error: only ") << actuallyRead << F(" bytes read!") << ENDL;
      numErrors++;
    }
    for (int i = 0; i < chunkSize; i++) {
      if (data[i] != value) {
        Serial << ENDL << F("*** Error: byte ") << i << F(" should be ") << value << F(". Found ") << data[i] << ENDL;
        numErrors++;
      }
      value++;
    } // for
    totalRead += chunkSize;
  } // while
  Serial << F("Done.") << ENDL;
}

void setup(void)
{
  Serial.begin(19200);
  randomSeed(analogRead(0));
  Wire.begin();
  Wire.setClock(400000L);

  Serial << F("Starting test...") << ENDL;

   testByteReadWrite();
   testPageWrite_SmallData();
   testPageWrite_LargeData();
   testWriteLastBytesOfChip();

  if (numErrors > 0) {
    Serial << F("Test over. *** ") << numErrors << F(" errors ***") << ENDL;
  }
  else {
    Serial << F("Test over. No error") << ENDL;
  }
}

void loop() {}

