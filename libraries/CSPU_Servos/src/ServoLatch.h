#pragma once
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
/* v1.1.8 of the Servo library does not support the SAMD51 processor. See note in class comment */
#include <Servo.h>
#else
#error "ServoLatch class not supported on current board."
#endif
#include "Arduino.h"
#include "Latch.h"

/** @ingroup GMiniCSPU
    @brief Implementation of a Latch based on a servo.
    The class has been tested with:
    - micro servo (SG90) which requires non-default values for minPulseWidth and maxPulseWidth.
    - micro servo (SG92R) which works perfectly with the default minPulseWidth and maxPulseWidth
    values.
*/

class ServoLatch : public Latch {
  public :
    virtual ~ServoLatch() {};

    /** Method to call before any other method. Initializes the servo.
        @param PWM_Pin the PWM Pin number of the servo
        @param enablePin The pin used to activated the Servo (active HIGH). If 0, it is
               assumed that the Servo is always active. If an enable pin is provided, it is
               used to enable/disable the servo every time it is attached/detached.
        @param minPulseWidth The minimum pulse width to be used with the servo.
        @param maxPulseWidth The maximum pulse width to be used with the servo.
        @return true if servo initialisation succeeded
    */
    bool begin (const uint8_t PWM_Pin, uint8_t enablePin = 0, int minPulseWidth = 544, int maxPulseWidth = 2400);
    virtual void unlock() override;
    virtual void lock() override;
    virtual void neutral() override; /**< Method for returning the Servo to the neutral position*/
    void configure(int neutralPosition, int amplitude);

  protected:
    /** Enable/Disable the servo using the "enable" pin.
        If no enable pin is used, the method does do anything.
        @param status True to enable, false to disable.
    */
    void enableServo(bool status);

  private :
    Servo myServo; 			 /**< The arduino Servo object*/
    int unlockPosition = 30; /**< Position (in degrees) of the servo when unlocked*/
    int lockPosition = 0; 	 /**< Position (in  degrees) of the servo when locked*/
    static constexpr uint16_t DelayBeforeDetach = 200;  /**< Delay (in millisecond) of waiting before detach*/
    static constexpr uint16_t DelayAfterAttach = 200;   /**< Delay (in millisecond) of waiting after attach*/
    uint8_t thePWM_Pin; 	 /**< Id of the pin used for pulsating the servo. */
    uint8_t theEnablePin; 	 /**< Id of the pin used to enable the servo, if any (0=none). */
    int theMinPulseWidth;  	 /**< Declaration of the theMinPulseWidth */
    int theMaxPulseWidth;  	 /**< Declaration of the theMaxPulseWidth */
};
