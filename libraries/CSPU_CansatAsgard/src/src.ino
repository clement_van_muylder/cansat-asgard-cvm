/* Dummy file to allow editing the library in the Arduino IDE.
 * See https://arduino.stackexchange.com/questions/14189/how-to-develop-or-edit-an-arduino-library for details
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
