/*
   IsaTwoImager.cpp
*/
#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_BEGIN 1		  // Define for detailed debugging msg during initialisation
#define DBG_CAPTURE 0	  // Define for detailed debugging msg during capture (including timing)
#define DBG_SAVE 0		  // Define for detailed debugging msg while transferring from camera to SD card.
#define DBG_SAVE_WITH_DETAILS 0 // Define to output every single byte transfered from the camera
#define DBG_PRINT_SUMMARY 0 // Define to output terse summary of operation (file name etc.)
#define DBG_BUILD_DIR_NAME 0 // Define for detailed debugging of directory selection.
#define DBG_DIAGNOSTIC 1  // Define to have message in case of error only.

#define CHECK_CAMERA_MODEL  // Define to check camera model in begin. It requires I2C bus but so
							// does the call to myCam.init().

#include "IsaTwoImager.h"
#include "SPI.h"
#include "IsaTwoConfig.h"

#define TEST_FILE_NAME "__Test__.tst"  // File created/delete to test SD card. 

constexpr byte NumSPI_Retries=5; // Number of retries when checking camera availability

/*   *******************************************
 *   ***            PUBLIC METHODS           ***
 *   *******************************************/
IsaTwoImager::IsaTwoImager(byte cameraChipSelect,uint32_t cameraSPI_Speed)
  : myCAM(OV2640, cameraChipSelect), spiCamSettings(cameraSPI_Speed, MSBFIRST, SPI_MODE0) {}

bool IsaTwoImager::begin(byte SD_chipSelect, uint8_t imageSize)
{
  DPRINTSLN(DBG_BEGIN, "Isa2Imager.begin()");

  // 1. Check the SD card is available (otherwise fail)
  if (!mySD.begin(SD_chipSelect)) {
    DPRINTS(DBG_DIAGNOSTIC, "*** SD.begin() failed. CS=");
    DPRINTLN(DBG_DIAGNOSTIC, SD_chipSelect);
    return false;
  } else {
	DPRINTSLN(DBG_BEGIN, "SD-Card init OK");
  }
  
  // Identify and create our destination directory
  unsigned int counter = 1;
  do {
      buildDirName("2Img", counter);
      counter++;
    } while (mySD.exists(dirName.c_str()));
  if (!mySD.mkdir(dirName.c_str())) {
	  DPRINTS(DBG_DIAGNOSTIC, "Can not create directory ");
	  DPRINTLN(DBG_DIAGNOSTIC, dirName);
	  return false;
  }
  DPRINTSLN(DBG_BEGIN, "Directory created");

  if (!mySD.chdir(dirName.c_str())) {
  	  DPRINTS(DBG_DIAGNOSTIC, "Cannot cd to ");
  	  DPRINTLN(DBG_DIAGNOSTIC, dirName);
  	  return false;
    }
  // From now one, everything happens in our new directory.
  DPRINTS(DBG_DIAGNOSTIC, "Files will be stored in directory: ");
  DPRINTLN(DBG_DIAGNOSTIC, dirName);
  blinkBuiltInLED();

  // Check we can actually create a file
  if (!checkFileCreation()) {
    return false;
  }

  blinkBuiltInLED();

  // 2. Check a camera is indeed connected to the SPI bus.
  if (!checkSPI_Bus()) {
	  return false;
  }

  blinkBuiltInLED();

#ifdef CHECK_CAMERA_MODEL
  // 3. Check we find the appropriate camera
  //    This uses the I2C bus which is better checked: it is used for the camera init
  //    as well...
  if (!checkCamera()) {
	  return false;
  }
  blinkBuiltInLED();
#endif

  // 4. Configure the camera to deliver images as JPEG files
  SPI.beginTransaction(spiCamSettings);
  myCAM.CS_LOW();
  delay(5);
  blinkBuiltInLED();
  myCAM.set_format(JPEG);
  blinkBuiltInLED();
  delay(5);
  myCAM.InitCAM();   			// Warning: call AFTER set_format.
  blinkBuiltInLED();
  SPI.endTransaction();
  myCAM.CS_HIGH();
  delay(5);
  blinkBuiltInLED();
  setImageSize(imageSize);

  // Set default configuration.
#if defined (OV2640_MINI_2MP) || defined(OV2640_MINI_2MP_PLUS)
 myCAM.OV2640_set_Light_Mode(IsaTwoLightMode);
 myCAM.OV2640_set_Color_Saturation(IsaTwoColorSaturation);
 myCAM.OV2640_set_Brightness(IsaTwoBrightness);
 myCAM.OV2640_set_Contrast(IsaTwoContrast);
#else
 DPRINTSLN(DBG_DIAGNOSTIC, "Warning: not using OV2640_MINI_2MP[_PLUS]. Could not set configuation.");
#endif

  blinkBuiltInLED();

  DPRINTSLN(DBG_BEGIN, "Camera Setup ok.");
  return true;
}

void IsaTwoImager::setImageSize(uint8_t imageSize) {
  SPI.beginTransaction(spiCamSettings);
  myCAM.CS_LOW();
#if defined (OV2640_MINI_2MP) || defined(OV2640_MINI_2MP_PLUS)
  myCAM.OV2640_set_JPEG_size(imageSize);
#elif defined (OV3640_MINI_3MP)
  myCAM.OV3640_set_JPEG_size(OV3640_320x240);
#else
  myCAM.write_reg(ARDUCHIP_TIM, VSYNC_LEVEL_MASK);   //VSYNC is active HIGH
  myCAM.OV5642_set_JPEG_size(OV5642_320x240);
#endif
  SPI.endTransaction();
  myCAM.CS_HIGH();
}

bool IsaTwoImager::image(unsigned long timestamp)
{
  bool result = false;
  char fileName[13];
  bool ok=captureImage();
  if (!ok) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "Error during capture (Aborted).");
	  return false;
  }
  buildFileName(fileName, timestamp);
  result = saveImageToFile(fileName);
  if (!result) {
    mySD.remove(fileName);
    delay(1000);
  }
  else
  {
    DPRINTS(DBG_PRINT_SUMMARY, "Saved to file ");
    DPRINTLN(DBG_PRINT_SUMMARY, fileName);
  }
  return result;
}

/*   **********************************************
 *   ***            PROTECTED METHODS           ***
 *   **********************************************/

void IsaTwoImager::buildDirName(const char* fourCharPrefix, const unsigned int number) {
	dirName = fourCharPrefix;
	DASSERT(dirName.length() == 4);

	char numStr[5];
	sprintf(numStr, "%04d", number);

	dirName += numStr;

	DPRINTS(DBG_BUILD_DIR_NAME, "Name of the directory: ");
	DPRINTLN(DBG_BUILD_DIR_NAME, dirName);
	// Do not used DPRINTSLN above to avoid wrapping the String in macro F()

}

bool IsaTwoImager::checkFileCreation() {
  myCAM.CS_HIGH(); // Unselect camera, just to be sure
  mySD.remove(TEST_FILE_NAME);
  File tst = openSD_File(TEST_FILE_NAME);
  if (!tst) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Could not open test file on SD card.");
    return false;
  } else {
    DPRINTSLN(DBG_BEGIN, "Test file opened on SD card.");
    if (tst.write(5) != 1) { // write anything to avoid empty file.
      DPRINTSLN(DBG_DIAGNOSTIC, "Error writing to test file");
    }
  }
  tst.close();
  mySD.remove(TEST_FILE_NAME);
  return true;
}

bool IsaTwoImager::checkSPI_Bus() {
  uint8_t temp;
  bool result=false;
  int i = NumSPI_Retries;
  while ((i>0) && !result) {
    //Check if the ArduCAM SPI bus is OK
    SPI.beginTransaction(spiCamSettings);
    myCAM.CS_LOW();
    myCAM.write_reg(ARDUCHIP_TEST1, 0x55);
    temp = myCAM.read_reg(ARDUCHIP_TEST1);
    myCAM.CS_HIGH();
    SPI.endTransaction();
    DPRINTS(DBG_BEGIN, "SPI: Wrote 0x55... Read again: 0x");
    DPRINTLN(DBG_BEGIN, temp, HEX);
    if (temp != 0x55) {
      DPRINTSLN(DBG_DIAGNOSTIC, "SPI interface Error! Retrying...");
      delay(1000);
      i--;
    } else {
      DPRINTSLN(DBG_BEGIN, "SPI interface OK.");
      result=true;
    }
  }
  return result;
}

bool IsaTwoImager::checkCamera() {
	bool result=false;
	int i = 0;
#if defined (OV2640_MINI_2MP) || defined(OV2640_MINI_2MP_PLUS)
  DPRINTSLN(DBG_BEGIN, "Checking for OV2640...");
  uint8_t vid, pid;
  while ((i<NumSPI_Retries) && !result) {
    //Check if the camera module type is OV2640
	// NB: This happens through I2C bus, so why is the transaction and CS required??
	//     This code is from Arducam, though....
    SPI.beginTransaction(spiCamSettings);
    myCAM.CS_LOW();
    delay(5);
    myCAM.wrSensorReg8_8(0xff, 0x01);
    delay(5);
    myCAM.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
    delay(5);
    myCAM.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
    myCAM.CS_HIGH();
    SPI.endTransaction();
    if ((vid != 0x26) && ((pid != 0x41) || (pid != 0x42))) {
      DPRINTSLN(DBG_DIAGNOSTIC, "Can't find OV2640 module on I2C! (retrying)");
      delay(1000);
      i++;
    } else {
      DPRINTSLN(DBG_DIAGNOSTIC, "OV2640 detected.");
      result=true;
    }
  }
#elif defined (OV3640_MINI_3MP)
  while ((i<NumSPI_Retries) && !result) {
    //Check if the camera module type is OV3640
    SPI.beginTransaction(CAMERA_SPI_SETTINGS);
    myCAM.CS_LOW();
    myCAM.rdSensorReg16_8(OV3640_CHIPID_HIGH, &vid);
    myCAM.rdSensorReg16_8(OV3640_CHIPID_LOW, &pid);
    myCAM.CS_HIGH();
    SPI.endTransaction();
    if ((vid != 0x36) || (pid != 0x4C)) {
      DPRINTSLN(DBG_BEGIN,  "Can't find OV3640 module!");
      delay(1000);//delay à supprimer ?
      i++;
    } else {
      DPRINTSLN(DBG_BEGIN,  "OV3640 detected.");
      result=true;
    }
  }
#else
  while ((i<NumSPI_Retries) && !result) {
    //Check if the camera module type is OV5642
    SPI.beginTransaction(spiCamSettings);
    myCAM.CS_LOW();
    myCAM.wrSensorReg16_8(0xff, 0x01);
    myCAM.rdSensorReg16_8(OV5642_CHIPID_HIGH, &vid);
    myCAM.rdSensorReg16_8(OV5642_CHIPID_LOW, &pid);
    myCAM.CS_HIGH();
    SPI.endTransaction();
    if ((vid != 0x56) || (pid != 0x42)) {
      DPRINTSLN(DBG_BEGIN,  "Can't find OV5642 module!");
      delay(1000); //delay à supprimer ?
      i++;
    }
    else {
      DPRINTSLN(DBG_BEGIN,  "OV5642 detected.");
      result=true;
    }
  }
#endif
   return result;
}

bool IsaTwoImager::captureImage()
{
  SPI.beginTransaction(spiCamSettings);
  myCAM.CS_LOW();
  myCAM.flush_fifo();    //Flush the FIFO queue to remove any remain from previous transfer.
  myCAM.clear_fifo_flag();  //Clear the "transfer done" flag

#if (DBG_CAPTURE==1)
  DPRINTS(DBG_CAPTURE, "Starting capture...");
#endif

  unsigned long startTS = millis();
  myCAM.start_capture();
  while (!myCAM.get_bit(ARDUCHIP_TRIG , CAP_DONE_MASK)) {
    delay(5);  // to remove the capture in 0 msec which are always corrupt.
  }
  myCAM.CS_HIGH();
  SPI.endTransaction();
  unsigned long endCaptureTS = millis();

#if (DBG_CAPTURE==1)
  DPRINTS(DBG_CAPTURE, "done in ");
  DPRINT(DBG_CAPTURE, endCaptureTS - startTS);
  DPRINTSLN(DBG_CAPTURE, " msec");
#endif
  if ((endCaptureTS - startTS) < 5) {
    DPRINTSLN(DBG_DIAGNOSTIC, " *** Capture in no time: image discarded!");
    return false;
  }
  return true;
}

void IsaTwoImager::buildFileName(char* buffer, unsigned long timestamp)
{
  // print into the provided buffer, which is expected to be at least 13 characters long.
  // % introduces a variable
  // 0 to have leading zero's, 8 to use 8 characters for the timestamp, lu to denote a long unsigned integer.
  // .jpg is just copied to the buffer.
  // NB: timestamp reaches 99 999 999 msec after 27 hours: OK.
  sprintf(buffer, "%08lu.jpg", timestamp);
  DPRINTS( DBG_SAVE, "FileName=");
  DPRINTLN(DBG_SAVE, buffer);
}

File IsaTwoImager::openSD_File(const char* fileName) {
  //Open the new file
  File outFile = mySD.open(fileName, O_WRITE | O_CREAT | O_TRUNC);
  // O_TRUNC causes an existing file to be truncated to zero length and rewritten.
  if (!outFile) {
    DPRINTS(DBG_DIAGNOSTIC, "File open failed for ");
    DPRINTLN( DBG_DIAGNOSTIC, fileName); //use DPRINT, not DPRINTS with non constant strings.
    DPRINTSLN(DBG_DIAGNOSTIC, " (aborted).");
  } else {
    DPRINTS(DBG_SAVE, "Opened file for the images ");
    DPRINTLN(DBG_SAVE, fileName);
  }
  return outFile;
}

void IsaTwoImager::blinkBuiltInLED() {
	digitalWrite(LED_BUILTIN, HIGH);
	delay(200);
	digitalWrite(LED_BUILTIN, LOW);
	delay(200);
}

bool IsaTwoImager::saveImageToFile(const char* fileName)
{
  byte imageBuffer[256];		// The buffer we accumulate before writing to file.
  int bufferIndex = 0;	 		// The index of the first free position in the buffer.

  uint8_t temp = 0;					        // The last byte received
  uint8_t previousTemp = 0;			    // The byte received just before the last one.
  uint32_t bytesToTransfer = 0;		  // The total number of bytes we have to receive from the camera.
  uint32_t bytesLeftToTransfer = 0;	// The number of bytes still to be received from the camera.
  bool inImage = false;   			    /* This variable will be true when the beginning of the header
                                       has been received, but the end of image not detected */
  bool headerEncountered = false; 	// Will be true as soon as the header has been detected.
  int written;						          // Used to store the number of bytes actually written to SD

  // 1. Open new file. If it fails, we just return.
  File outFile = openSD_File(fileName);
  if (!outFile) {
    return false;
  }
  else
  {
    DPRINTS(DBG_SAVE, "Opened file ");
    DPRINTLN(DBG_SAVE, fileName);
  }

  // 2. Obtain the number of bytes to transfer
  myCAM.CS_LOW();
  SPI.beginTransaction(spiCamSettings);
  bytesLeftToTransfer = bytesToTransfer = myCAM.read_fifo_length();
  SPI.endTransaction();
  myCAM.CS_HIGH();

  if (bytesToTransfer >= MAX_FIFO_SIZE) //384K
  {
    DPRINTSLN(DBG_DIAGNOSTIC, "FIFO oversize (aborted).");
    return false;
  }
  if (bytesToTransfer == 0 ) //0 kb
  {
    DPRINTSLN(DBG_DIAGNOSTIC, "FIFO size=0 (aborted)");
    return false;
  }
  DPRINT(DBG_SAVE, bytesLeftToTransfer );
  DPRINTSLN(DBG_SAVE, " bytes to transfer");

  // 3. Prepare camera for transfer
  SPI.beginTransaction(spiCamSettings);
  myCAM.CS_LOW();       // select ArduCAM on SPI bus.
  myCAM.set_fifo_burst();  // Enter burst read mode: every time we'll transfer 0x00, we'll get
  // 1 byte of data. First one is a dummy byte and is just discarded
  // 0xFF followed by 0xD9 is the end of file flag
  // 0xFF followed by 0xD8 is the beginning of the header (hence the beginning of the data).

  // 4. Read and transfer bytes (number of available bytes is in length).
  while (bytesLeftToTransfer-- )
  {
    DPRINT(DBG_SAVE_WITH_DETAILS, bytesLeftToTransfer );
	DPRINTS(DBG_SAVE_WITH_DETAILS, " bytes left: ");

    // Read a single byte from SPI, after saving the previously received one.
    previousTemp = temp;
    temp =  SPI.transfer(0x00);
    DPRINTS(DBG_SAVE_WITH_DETAILS, "  prev=0x");
    DPRINT( DBG_SAVE_WITH_DETAILS, previousTemp, HEX);
    DPRINTS(DBG_SAVE_WITH_DETAILS, " tmp=0x");
    DPRINT( DBG_SAVE_WITH_DETAILS, temp, HEX);

    if (!inImage) {
      DPRINTSLN(DBG_SAVE_WITH_DETAILS, "  Not in image");
      // *** CASE 1 ***: Header not detected yet. Just check whether it is arrived.
      if ((temp == 0xD8) && (previousTemp == 0xFF))
      {
        // Header is detected. We are receiving the first meaningful bytes
        DPRINTSLN(DBG_SAVE, "Entering header...");
        inImage = true;
        headerEncountered = true;
        imageBuffer[bufferIndex++] = previousTemp;
        imageBuffer[bufferIndex++] = temp;
      }
    }
    else
    {
      // *** CASE 2 ***:  We are inside the image
      // Write image data to buffer if not full. Last valid index is 255 (256 bytes = index from 0 to 255)
      // so we can add a character in the buffer as long as bufferIndex < 255.
      // otherwise we write the buffer to SD, and store the new byte afterwards.
      DPRINTSLN(DBG_SAVE_WITH_DETAILS, "  In image");
      if (bufferIndex < 255)
      {
        imageBuffer[bufferIndex++] = temp;
      }
      else
      {
        // Here, bufferIndex is 255. There is one place left in buffer.
        imageBuffer[bufferIndex++] = temp;
        // Now, buffer is full
        // Write 256 bytes image data to file
        DPRINTS(DBG_SAVE, "Writing 256 bytes to file...");
        SPI.endTransaction();   // End transaction with camera, we need to access the SD card.
        myCAM.CS_HIGH();		// Deselect camera while accessing SD card.
        written = outFile.write(imageBuffer, 256);
        if (written != 256) {
          DPRINTS(  DBG_DIAGNOSTIC, "Error: only wrote :");
          DPRINT(   DBG_DIAGNOSTIC, written);
          DPRINTSLN(DBG_DIAGNOSTIC, "bytes");
        } else {
          DPRINTSLN(DBG_SAVE, "OK");
        }
        bufferIndex = 0;
        SPI.beginTransaction(spiCamSettings);
        myCAM.CS_LOW();			// Reselect camera
        myCAM.set_fifo_burst();	// Resume transfer.
      }

      if ( (temp == 0xD9) && (previousTemp == 0xFF) )
      {
        // End of image reached.
        //Write the remaining bytes from the buffer and close file.
        DPRINTS(DBG_SAVE, "End reached. Writing the last ");
        DPRINT(DBG_SAVE, bufferIndex);
        DPRINTS(DBG_SAVE, " bytes...");
        SPI.endTransaction();
        myCAM.CS_HIGH();
        written = outFile.write(imageBuffer, bufferIndex);
        if (written != bufferIndex) {
          DPRINTS(  DBG_DIAGNOSTIC, "Error: only wrote :");
          DPRINT(   DBG_DIAGNOSTIC, written);
          DPRINTSLN(DBG_DIAGNOSTIC, "bytes");
        } else {
          DPRINTSLN(DBG_SAVE, "OK");
        }
        outFile.close();
        DPRINTS(DBG_SAVE, "Image save OK: numBytes=");
        DPRINTLN(DBG_SAVE, bytesToTransfer);
        inImage = false;
        bytesLeftToTransfer = 0; // make sure we exit the loop: we are not interested in any additional bytes.
      } // End detection of end of image.
    }
  } // while

  // 5. Check the resulting file is valid. This is not the case if we never detected the header and
  //    if we never detected the end of file. If we return false, the caller will remove the file, if any.
  if (!headerEncountered) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: never got the start of file OxFF - OxD9");
    SPI.endTransaction();
    myCAM.CS_HIGH(); // Make sure we release the bus.
    outFile.close(); //Close the file and remove (empty)
    return false;
  }
  if  (inImage ) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: got the whole fifo, but no 0xFF - 0xD8 as end of file marker")
    SPI.endTransaction();
  	myCAM.CS_HIGH(); // Make sure we release the bus.

    outFile.close(); //Close the file (probably corrupt? ).
    delay(2000);
    return false;
  }
  return true;
}
