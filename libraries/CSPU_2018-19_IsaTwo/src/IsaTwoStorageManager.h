/*
 *  IsaTwoStorageManager.h
 *  A class extending StorageManager specifically for the ISATWO project
 *   
 */

#pragma once

#include "StorageManager.h"
#include "IsaTwoRecord.h"

class  IsaTwoStorageManager: public StorageManager {
public:
	IsaTwoStorageManager(unsigned int theCampainDurationInSec, unsigned int theMinStoragePeriodInMsec);
	/* Perform the ISATWO-specific part of data storage, after the storage manager has validated that acquisition was performed successfully
	 * and the data is relevant (as evaluated by the dataRelevant() method. */
	virtual ~IsaTwoStorageManager() {};
	NON_VIRTUAL void storeIsaTwoRecord(const IsaTwoRecord& data,const bool useEEPROM);
};

