/*
   ThermistorSteinhartHart.h
*/
#pragma once

#include "Arduino.h"
#include "Thermistor.h"
/**
 * @ingroup CSPU_Thermistors
 * @brief   This class calculates the temperature measured using a thermistor.
 * 			Temperature is derived from the resistance of the thermistor
 *			using the Steinhart-Hart model, which requires 5 parameters:
 *			constants A, B, C, D and the reference resistance at 25°C.
 *			More details in the project reports (e.g. SpySeeCan report II)
 * 			Wiring: from Vcc to a resistor (serial resistor), to the thermistor,
 *			to the ground.  Voltage is measured at the middle point using an
 *			analog pin from the µController.
 */
class ThermistorSteinhartHart : public Thermistor {
  public:
  /**
   * @param theAnalogPinNbr this is the number of the analog pin
   * @param theRefResistanceAt25 this is the resistor at 25 degrees
   * @param theA is the first parameter used to calculate the temperature
   * @param theB is the second parameter used to calculate the temperature
   * @param theC is the third parameter used to calculate the temperature
   * @param theD is the fourth parameter used to calculate the temperature
   * @param theSerialResistor this is the value of the serial resistor, in ohms.
   */
    ThermistorSteinhartHart(	const float theVcc,
    					const uint8_t theAnalogPinNbr,
						const float theRefResistanceAt25,
						const float theA, const float theB,
						const float theC, const float theD,
						const float theSerialResistor);
    virtual ~ThermistorSteinhartHart() {};
    /** Obtain the temperature by reading the voltage (averaging several readings),
     *  derive the resistance of the thermistor and then the corresponding temperature.
     *  @return The resulting temperature in °C.
     */
    virtual float readTemperature()const;
  private:
    float A, B, C, D;
    float r25;
};
