/*
   WormScrewLatch.h
*/

#pragma once

#include "Arduino.h"
#include "Latch.h"

/** @ingroup CSPU_Motors
    @brief Implementation of a Latch based on a worm screw and a motor power using a H-bridge.
    The class has been tested with:
    - Motor TO BE COMPLETED
    - H-Bridge L923D.
*/

class WormScrewLatch : public Latch {

  public :

    /** Method to call before any other method. Initializes whatever must be.
        TO BE ADAPTED: change parameters for motor and H-bridge control.
        @param PWM_Pin the PWM Pin number of the servo
        @param enablePin The pin used to activated the Servo (active HIGH). If 0, it is
               assumed that the Servo is always active. If an enable pin is provided, it is
               used to enable/disable the servo every time it is attached/detached.
        @param minPulseWidth The minimum pulse width to be used with the servo.
        @param maxPulseWidth The maximum pulse width to be used with the servo.
        @return true if servo initialisation succeeded
    */
    bool begin(uint8_t ScrewLatchPWM_Pin, uint8_t ScrewLatchForward_Pin, uint8_t ScrewLatchReverse_Pin);
    virtual void unlock() override;
    virtual void lock() override;
    void TurnForward (uint8_t DelayForward);
    void TurnBackward (uint8_t DelayBackwards);
    WormScrewLatch () ;

    // A ADAPTER void configure(int neutralPosition,int amplitude);

    //protected:

  private :

    uint8_t ScrewLatchPWM_Pin;
    uint8_t ScrewLatchForward_Pin;
    uint8_t ScrewLatchReverse_Pin;
    uint8_t DelayForward;
    uint8_t DelayBackwards;
    uint8_t Power = 255;
    const uint8_t theScrewLatchForward_Pin;
    const uint8_t theScrewLatchPWM_Pin;
    const uint8_t theScrewLatchReverse_Pin;
    const uint8_t theDelayForward;
    const uint8_t theDelayBackwards;

};
