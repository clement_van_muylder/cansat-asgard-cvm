/*
   WormScrewLatch.cpp
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOCK 0
#define DBG_UNLOCK 0
#define DBG_BEGIN 0
#include "WormScrewLatch.h"

/* A ADAPTER
  bool WormScrewLatch::begin( const uint8_t PWM_Pin,  uint8_t enablePin, int minPulseWidth, int maxPulseWidth) {
  thePWM_Pin = PWM_Pin;
  theMinPulseWidth = minPulseWidth;
  theMaxPulseWidth = maxPulseWidth;
  theStatus=ServoLatch::LatchStatus::Undefined;
  DPRINTS(DBG_BEGIN, "minPulseWidth=");
  DPRINTLN(DBG_BEGIN, minPulseWidth );
  DPRINTS(DBG_BEGIN, "maxPulseWidth=");
  DPRINTLN(DBG_BEGIN, maxPulseWidth );

  theEnablePin=enablePin;
  if (enablePin != 0) {
	  pinMode(enablePin, OUTPUT);
  }
  if (!AttachDetachEveryTime) {
	enableServo(true);
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth );
    DPRINTLN(DBG_BEGIN, "Attaching");
  }
  return true;
  }
*/

bool WormScrewLatch::begin(uint8_t ScrewLatchPWM_Pin, uint8_t ScrewLatchForward_Pin, uint8_t ScrewLatchReverse_Pin) {
  const uint8_t theScrewLatchForward_Pin = ScrewLatchForward_Pin;
  const uint8_t theScrewLatchReverse_Pin = ScrewLatchReverse_Pin;
  const uint8_t theScrewLatchPWM_Pin = ScrewLatchPWM_Pin;
  pinMode(theScrewLatchForward_Pin, OUTPUT);
  pinMode(theScrewLatchReverse_Pin, OUTPUT);
  pinMode(theScrewLatchPWM_Pin, OUTPUT);
  return true;
}

void WormScrewLatch::unlock() {
  // TO BE COMPLETED
  theStatus = LatchStatus::Unlocked;
}

void WormScrewLatch::lock() {
  //TO BE COMPLETED
  theStatus = LatchStatus::Locked;
}

void WormScrewLatch::TurnForward(uint8_t DelayForward) {
  Serial << "Motor turns forward during " << DelayForward << " milliseconds." << ENDL;
  const uint8_t theDelayForward = DelayForward;
  digitalWrite(theScrewLatchPWM_Pin, HIGH); // Active pont en H
  analogWrite(theScrewLatchReverse_Pin, 0);
  analogWrite(theScrewLatchForward_Pin, Power);
  delay(DelayForward);
  analogWrite(theScrewLatchForward_Pin, 0);
  analogWrite(theScrewLatchReverse_Pin, 0);
  digitalWrite(theScrewLatchPWM_Pin, LOW);
}

void WormScrewLatch::TurnBackward(uint8_t DelayBackwards) {
  Serial << "Motor turns backwards during " << DelayBackwards << " milliseconds." << ENDL;
  const uint8_t theDelayBackwards = DelayBackwards;
  digitalWrite(theScrewLatchPWM_Pin, HIGH); // Active pont en H
  analogWrite(theScrewLatchForward_Pin, 0);
  analogWrite(theScrewLatchReverse_Pin, Power);
  delay(DelayBackwards);
  analogWrite(theScrewLatchForward_Pin, 0);
  analogWrite(theScrewLatchReverse_Pin, 0);
  digitalWrite(theScrewLatchPWM_Pin, LOW);
}
