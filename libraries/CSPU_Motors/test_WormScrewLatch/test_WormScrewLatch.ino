#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "WormScrewLatch.h"

WormScrewLatch myScrew ;

void setup() {
  DINIT(115200);
  myScrew.begin(7, 2, 1);
}

void loop() {
  myScrew.TurnForward(1000);
  delay(2000);
  myScrew.TurnBackward(1000);
  delay(2000);
};
