#define CSPU_DEBUG
#include "DebugCSPU.h"
#include "GMiniRecordExample.h"
#include "CansatXBeeClient.h"

GMiniRecordExample exRecord;
GMiniRecord record;

constexpr uint8_t BufSize=80;
uint8_t exBuffer[BufSize]={0xFF}, gmBuffer[BufSize]={0xFF};
uint8_t exBufferSize=BufSize, gmBufferSize=BufSize;

void clearBuffer(uint8_t buffer[], uint8_t size) {
  for (int i=0; i< size ; i++)  buffer[i]=0xFF;
}

void printBuffer(uint8_t buffer[], uint8_t size) {
  for (int i=0; i< size ; i++) {
    if (buffer[i] < 0x10) Serial << '0';
    Serial.print(buffer[i], HEX);
    if (((i+1)%5) ==0) Serial << "  ";
    if (((i+1)%10) ==0) Serial << ENDL;
  }
  Serial << ENDL;
}

void printElementaryTypesSize() {
  Serial << "Type sizes:" << ENDL;
  Serial << "  unsigned int : " << sizeof(unsigned int) << ENDL;
  Serial << "       uint8_t : " << sizeof(uint8_t) << ENDL;
  Serial << "      uint16_t : " << sizeof(uint16_t) << ENDL;
  Serial << "      uint32_t : " << sizeof(uint32_t) << ENDL;
  Serial << "         float : " << sizeof(float) << ENDL;
  Serial << "        double : " << sizeof(double) << ENDL;
  Serial << "          char : " << sizeof(char) << ENDL;
  Serial << "           int : " << sizeof(int) << ENDL;
  Serial << "        int8_t : " << sizeof(int8_t) << ENDL;
  Serial << "       int16_t : " << sizeof(int16_t) << ENDL;
  Serial << "       int32_t : " << sizeof(int32_t) << ENDL;
  Serial << "          bool : " << sizeof(bool) << ENDL;
}

void initRecord(GMiniRecord& rec) {
  rec.clear();
  rec.timestamp = 123456;
  rec.newGPS_Measures=true;
  rec.GPS_LatitudeDegrees=98.76;
  rec.GPS_LongitudeDegrees=54.32;
  rec.GPS_Altitude=119;
#ifdef INCLUDE_GPS_VELOCITY
  rec.GPS_VelocityKnots=2.3;
  rec.GPS_VelocityAngleDegrees=4.5;
#endif

  // B. Primary mission data
 rec.temperatureBMP=18.6;
 rec.pressure=1024.8;
 rec.altitude=223;
#ifdef INCLUDE_REFERENCE_ALTITUDE
 rec.refAltitude=176;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
  rec.descentVelocity=12.3;
#endif
  rec.temperatureThermistor1=21.5;
#ifdef INCLUDE_THERMISTOR2
  rec.temperatureThermistor2=23.6;
#endif
#ifdef INCLUDE_THERMISTOR3
  rec.temperatureThermistor3=34.6
#endif
  rec.sourceID=CansatCanID::MainCan;
  rec.subCanEjected=true;
}

void setup() {
  DINIT(115200);
  initRecord(record);
  exRecord.clear();
  clearBuffer(gmBuffer, gmBufferSize);
  clearBuffer(exBuffer, exBufferSize);

  printElementaryTypesSize();
  Serial << "Announced binary sizes: " << record.getBinarySize() << " vs " << exRecord.getBinarySize() << ENDL;
  Serial << "Buffers initialized with all 0xFF values" << ENDL;

  
  Serial << "Writing to binary:" << ENDL;
  if (!record.writeBinary(gmBuffer, gmBufferSize)) {
    Serial << "  GMiniRecord failed" << ENDL;
  }
   if (!exRecord.writeBinary(exBuffer, exBufferSize)) {
    Serial << "  GMiniRecordExample failed" << ENDL;
  }

  Serial << "Buffers: " << ENDL;
  Serial << "GMiniRecord: " << ENDL;
  printBuffer(gmBuffer, gmBufferSize);
  Serial << ENDL << "GMiniRecordExample: " << ENDL;
  printBuffer(exBuffer, exBufferSize);
  Serial << ENDL;
  
  Serial << "Rereading:" << ENDL;
  if (!record.readBinary(gmBuffer, gmBufferSize)) {
    Serial << "  GMiniRecord failed" << ENDL;
  }
   if (!exRecord.readBinary(exBuffer, exBufferSize)) {
    Serial << "  GMiniRecordExample failed" << ENDL;
  }
   if (!exRecord.readBinary(gmBuffer, gmBufferSize)) {
    Serial << "  GMiniRecord into GMiniRecordExamplefailed" << ENDL;
  }
   if (!record.readBinary(exBuffer, exBufferSize)) {
    Serial << "  GMiniRecordExample into GMiniRecordExample failed" << ENDL;
  }
  Serial << "End of job" << ENDL; 
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
