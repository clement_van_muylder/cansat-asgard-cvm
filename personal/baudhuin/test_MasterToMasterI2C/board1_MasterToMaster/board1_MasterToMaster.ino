#include <Wire.h>

#define I2C_ADDRESS_OTHER 0x2
#define I2C_ADDRESS_ME 0x1

unsigned long counter = 0;
bool received = false;
bool receiving= true;

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial.println("board1_ MasterToMaster communication.");
  Serial.println("Starting as slave...");
  
  /* At startup we are acting as slave */
  Wire.begin(I2C_ADDRESS_ME);
  Wire.onReceive(receiveI2C);
}

void loop() {
  if (received) {
    if (receiving) {
      // Switch to emission
      Serial.println("Switching to emission (master)");
      Wire.begin();
      receiving=false;
    }
    Wire.beginTransmission(I2C_ADDRESS_OTHER);
    Wire.write("hello world from 0x1 to 0x2");
    Wire.endTransmission();
    if (Wire.endTransmission() == 0)  {
      // Switch to reception
      Serial.println("Switching to reception (slave)");
      receiving = true;
      received=false;
      Wire.begin(I2C_ADDRESS_ME);
      Wire.onReceive(receiveI2C);
    }
  }
  delay(1000);
}

// Interrupt routine: obviously operational software would not
// include calls to Serial.print! This is just a test. 
void receiveI2C(int howMany) {
  if (Wire.available()==0) {
    Serial.println("call to receiveI2C ignored");
  }
  Serial.print(counter++);
  Serial.print(": ");
  while (Wire.available() > 0) {
    char c = Wire.read();
    Serial.print(c);
  }
  received = true;
  Serial.println();
}
