/*
 * A class implementing a SOS message sent by a led.
 * 
 * Usage:
 * 
 *  MorseSOS theSOS(LED_BUILTIN, 5000);
 *  
 *  (in loop)
 *  theSOS.run();
 */
#include "MorseSign.h"

class MorseSOS {

  public:
    MorseSOS(const byte thePinNumber, unsigned int thePeriod) {
      state = Start;
      pinNumber=thePinNumber;
      period=thePeriod;
    }

    bool run() {
      bool charDone;
      switch (state) {
        case Start:
          aSign.start(pinNumber, MorseSign::Dot, 3);
          state = FirstChar;
          timestamp=millis();
          break;
        case FirstChar:
          charDone = aSign.run();
          if (charDone) {
            aSign.start(pinNumber, MorseSign::Dash, 3);
            state = SecondChar;
          }
          break;
        case SecondChar:
          charDone = aSign.run();
          if (charDone) {
            aSign.start(pinNumber, MorseSign::Dot, 3);
            state = ThirdChar;
          }
          break;
        case ThirdChar:
          charDone = aSign.run();
          if (charDone) {
            state = Done;
          }
          break;
        case Done:
          if ((millis()-timestamp) >= period) {
            state=Start;
          }
          break;
        default:
          Serial.println("Error: unexpected sos state");
      }
      return (state == Done);
    }

  private:
    enum SOS_Stage_t {
      Start,
      FirstChar,
      SecondChar,
      ThirdChar,
      Done
    };

    SOS_Stage_t state = Done;
    MorseSign aSign;
    byte pinNumber;
    unsigned int period;
    unsigned long timestamp;
};
