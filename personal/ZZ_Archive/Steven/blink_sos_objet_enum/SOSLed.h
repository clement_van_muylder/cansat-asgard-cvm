#pragma once    //To include all of this hearders only once
#include "Arduino.h"   //Include this headers to use a specific type of variable

class SOSLed{
  public:           //All that's written in public can be changed by anyone
    
    //Declaration of the constructor, functions and the run() method
    SOSLed(byte thePinNumber, uint32_t theDurationbwsos);
    void run();
    bool clicS();//This fonstion prints the letter S
    bool clicO();//This fonction prints the letter O
    bool pause();//This function does the pause between SOS
    

  private:          //All that's writen in private can't be changed by anyone

    //Declaration of the variables
    byte pinNumber;
    uint32_t durationBwSos;  //Variable for the duration between each SOS
    unsigned long ts;
    static constexpr uint32_t durationPoint = 300;
    static constexpr uint32_t durationUnderscore = 600;
    byte sosCounter = 0; 

    enum class state_t{
      first_s,
      middle_o,
      second_s,
      wait
    };
    state_t sosState;
};
