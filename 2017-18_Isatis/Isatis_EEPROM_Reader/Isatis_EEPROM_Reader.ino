
#include "IsatisHW_Scanner.h"
#include "EEPROM_BankWithTools.h"
#include "IsatisConfig.h"
#include "IsatisDataRecord.h"


IsatisHW_Scanner hw;
EEPROM_BankWithTools eb;

void setup() {
  Serial.begin (19200);

  hw.IsatisInit();
  hw.printFullDiagnostic(Serial);

  IsatisDataRecord buffer;
  bool result = eb.init(EEPROM_KeyValue, hw, sizeof(IsatisDataRecord));
  if (!result) {
    Serial.println("!!ERROR during init of EEPROM_Bank");
    Serial.flush();
    exit(0);
  }
  eb.printHeaderFromChip();

  unsigned long numLeft = eb.recordsLeftToRead();
  unsigned long numFree = eb.getNumFreeRecords();

  Serial.println();
  Serial.print("EEPROM header key      : 0x");
  Serial.println(EEPROM_KeyValue, HEX);
  Serial.print("Size of Isatis record  : ");
  Serial.print(sizeof(buffer));
  Serial.println(" bytes.");
  Serial.print("IsatisAcquisitionPeriod: ");
  Serial.print(IsatisAcquisitionPeriod);
  Serial.println(" msec.");
  Serial.print("Used space             : ");
  Serial.print(numLeft);
  Serial.print(" records = ");
  Serial.print((numLeft / 1000.0)*IsatisAcquisitionPeriod / 60.0);
  Serial.println(" min. of operation.");
  Serial.print("Free space             : ");
  Serial.print(numFree);
  Serial.print(" records = ");
  Serial.print((numFree / 1000.0)*IsatisAcquisitionPeriod / 60.0);
  Serial.println(" min. of operation.");

  Serial.println("----------");
  buffer.printCSV_Header(Serial);
  Serial << ENDL;


  while (eb.recordsLeftToRead() > 0) {
    eb.readOneRecord((byte*)&buffer, sizeof(IsatisDataRecord));
    buffer.printCSV(Serial);
    Serial << ENDL;
  }
  Serial.println("End of job");
}
void loop() {
  // put your main code here, to run repeatedly:

}
